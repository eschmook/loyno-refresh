<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>New Orleans | Loyola University New Orleans</title>
    <link type="text/css" rel="stylesheet" href="http://fast.fonts.net/cssapi/d5e07912-c037-4030-b268-eb9fd671dc66.css" />
    <!-- Bootstrap -->
    <link href="./css/bootstrap.css" rel="stylesheet">
    <!--<link rel="stylesheet" type="text/css" href="styles.css"> -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="./apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="./apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="./apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="./apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="./apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="./apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="./apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="./apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="./apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="./android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="./favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="./favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="./favicon-16x16.png">
    <link rel="manifest" href="./manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="./ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- rich preview meta -->
    <meta name="description" content="Home page of Loyola University New Orleans" />
    <meta property="og:title" content="Loyola University New Orleans Home Pagew" />
    <meta property="og:description" content="Loyola University New Orleans is the best." />
    <meta property="og:image" content="./img/loyno_havoc.jpg" />

    <link rel="stylesheet" type="text/css" href="./fonts/faith-and-glory-one/faith-and-glory-one.css">
    <link rel="stylesheet" type="text/css" href="./fonts/faith-and-glory-two/faith-and-glory-two.css">
    <link rel="stylesheet" href="./css/flickity.css" media="screen">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <?php require('includes/header.php'); ?>

    <div id="landing-neworleans" class="landingSection hero">
    <div id="whiteOverlay"></div>
        <div class="container">
            <div class="col-md-12">
            <h2>New Orleans </h2>
                <p>
                <strong>New Orleans is like no other city in the world. </strong><br />

                It’s where jazz was born and lives on. Where dining out is a religious experience. Where dressing in costume requires no reason at all. And with its buzzing tech and arts scenes, New Orleans is a place to make your mark.</p>
                <a href="#thingsToDo" class="slowscrolling"><img src="./img/svg/arrow-down.svg" alt="arrow to read more" class="arrow_more pulse_animation" /></a>

            </div>


            
        </div>
    </div>

    <div id="thingsToDo" class="landingSection">
        <div class="container items">
            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12">

                <div id="accordion" role="tablist" aria-multiselectable="true">
                <p>
                This is your chance to bring learning to life. From Loyola’s Uptown location, you’ll head out into New Orleans to apply what you’re learning, help others, and shape the city itself.</p>

                  <div class="card">
                    <div class="card-header collapsed" role="tab" data-toggle="collapse" data-parent="#accordion" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne" id="headerOne">Helping
                    </div>

                    <div id="collapseOne" class="collapse card-block" role="tabpanel" aria-labelledby="headerOne">
                      <ul>
                        <li>
                            <h6>Treat 1,000 Dental Patients</h6>
                            <p>Transform Mardi Gras World—a giant storage facility for parade floats— into a pop-up dental clinic.</p>
                        </li> 
                        <li>
                            <h6>Extend the Food Supply</h6>
                            <p>Plant fruit trees at the Acorn Farm in an urban food desert.</p>
                        </li>
                        <li>
                            <h6>Reduce Recidivism Rates</h6>
                            <p>Help nonprofit First 72+ build a database that assesses the needs of former inmates moving back into society.</p>
                        </li> 
                        <li>
                            <h6>Translate For Immigrants</h6>
                            <p>Be an interpreter to help local lawyers and immigration advocates aid an influx of unaccompanied minors fleeing Central America.</p>
                        </li> 
                        <li>
                            <h6>Work on the Modern Slavery Research Project</h6>
                            <p>Fight human trafficking and help homeless youth get off the streets.</p>
                        </li>                      
                        <li>
                            <h6>Design for Businesses</h6>
                            <p>Turn some heads! Design new brand identities and marketing materials for businesses ranging from restaurants to clothing stores.</p>
                        </li>  
                        </ul>
                    </div>
                  </div><!-- card -->

                  <div class="card">
                    <div class="card-header collapsed" role="tab" data-toggle="collapse" data-parent="#accordion" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" id="headerTwo">Teaching
                    </div>

                    <div id="collapseTwo" class="collapse card-block" role="tabpanel" aria-labelledby="headerTwo">
                      <ul>
                        <li>
                            <h6>Help Youth Improve Life Skills</h6>
                            <p>Volunteer with student-founded organization KEYNola to teach teenagers everything from managing their money to knowing their constitutional rights.</p>
                        </li> 
                        <li>
                            <h6>Demo How Science Works</h6>
                            <p>Teach kids how to make liquid nitrogen ice cream or how to power a light bulb with the turn of a crank.</p>
                        </li>
                        <li>
                            <h6>Give Tax Advice</h6>
                            <p>Assist low-income taxpayers in filing their taxes.</p>
                        </li> 
                        <li>
                            <h6>Volunteer in Local Schools</h6>
                            <p>Help first graders at ReNEW Cultural Arts Academy write—and publish—their own books.</p>
                        </li> 
                        <li>
                            <h6>Share Your Computer Skills</h6>
                            <p>Work as a computer literacy tutor at the city’s largest homeless shelter for men.</p>
                        </li>                      
                        <li>
                            <h6>Get Philosophical</h6>
                            <p>Use games and art to engage elementary students in deep questions—like “What do Aristotle’s Poetics and The Wizard of Oz have in common?”—during a meeting of the Philosopher Kids group.</p>
                        </li>  
                        </ul>
                    </div>
                  </div><!-- card -->

                  <div class="card">
                    <div class="card-header collapsed" role="tab" data-toggle="collapse" data-parent="#accordion" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree" id="headerThree">Creating
                    </div>

                    <div id="collapseThree" class="collapse card-block" role="tabpanel" aria-labelledby="headerThree">
                      <ul>
                        <li>
                            <h6>Build Your Own App</h6>
                            <p>Create a cool hiking app for a nature institute.</p>
                        </li> 
                        <li>
                            <h6>Report the Latest Local News</h6>
                            <p>Write stories, shoot video, and collaborate on breaking news with The Times-Picayune, New Orleans’ largest newspaper.</p>
                        </li>
                        <li>
                            <h6>Teach French Through Design</h6>
                            <p>Turn second-graders’ French stories into subtitled animations.</p>
                        </li> 
                        <li>
                            <h6>Volunteer for Mission and Ministry</h6>
                            <p>Plan a Passover Seder, Ramadan Iftar dinner, or Awakening Retreat for the student community.</p>
                        </li> 
                        <li>
                            <h6>Roll Out A Campaign</h6>
                            <p>Build a PR campaign encouraging local art vendors to use a new person-to-person payment system to increase profits (and avoid service fees).</p>
                        </li>                      
                        <li>
                            <h6>Plan, Produce, and Perform</h6>
                            <p>Tackle everything from promotions to setlists to performing for student-run shows at the legendary</p>
                        </li>  
                        </ul>
                    </div>
                  </div><!-- card -->

                </div><!-- accordion -->
            </div>

            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 stats">
              <div class="stat black">
                <span class="fa fa-plane"></span>
                  <div class="numberFact">#5</div>
                  <div>City Where Millennials Are Moving <em>Time</em></div>
              </div>

              <div class="stat maroon">
                  <span class="fa fa-lightbulb-o"></span>
                    <div class="numberFact">#1</div>
                    <div>Best New Brainpower Cities <em>Forbes</em></div>
              </div>    
            </div>
            
        </div><!-- container -->
    </div><!-- thingsToDo -->


<div id="cityGuide" class="featuredSection">
    <div class="container">
            <div class="col-md-12 col-xs-12">
                <h3>City Guide </h3>
                
            <div class="college-carousel">
                <div class="hovereffect carousel-cell nola-tech flipme featured-factoid">
                    <figure>
                        <figcaption>
                            <h5>NOLA Tech Scene</h5>
                            <p>New Orleans—voted #3 best city for tech jobs (Forbes) and coolest startup city in America (Inc.com)—is outpacing the rest of the nation. The Silicon Bayou’s tech scene spans from the IDEAcorps MBA consulting challenge to New Orleans Entrepreneur Week. </p>
                            
                        </figcaption>
                    </figure>
                    <h4>NOLA Tech Scene</h4>
                </div>

                <div class="hovereffect carousel-cell nola-food flipme featured-factoid">
                   <figure>
                        <figcaption>
                            <h5>NOLA Food Scene</h5>
                            <p>Sample 28 James Beard-award-winning chefs and restaurants within 6 miles of campus—all before graduation!</p>
                            
                        </figcaption>
                    </figure>                    
                    <h4>NOLA Food Scene</h4>
                </div>

                <div class="hovereffect carousel-cell nola-music flipme featured-factoid">
                    <figure>
                        <figcaption>
                            <h5>NOLA Music Scene</h5>
                            <p>NOLA’s fabled festivals extend from EDM to indie: Essence Festival, New Orleans Jazz and Heritage Festival, BUKU Music + Art Project, Voodoo Music + Arts Experience, French Quarter Festival, and more. </p>
                            
                        </figcaption>
                    </figure>                      
                    <h4>NOLA Music Scene</h4>
                </div>

                <div class="hovereffect carousel-cell nola-sports flipme featured-factoid">
                    <figure style="background-position: top;">
                        <figcaption>
                            <h5>NOLA Sports Scene</h5>
                            <p>Action-packed sports events entertain you all year—from the Allstate Sugar Bowl to the Crescent City Classic 10k. Fly your flag for the Saints (football), Pelicans (basketball), Zephyrs (baseball), Jesters (soccer), and the Loyola Wolf Pack.</p>
                            
                        </figcaption>
                    </figure>                      
                    <h4>NOLA Sports Scene</h4>
                </div>

            </div><!-- end pack-carousel -->

        </div>
    </div><!-- container -->

</div>

<div id="exploreBanner" class="landingSection">
    <div class="container">
        <div class="col-md-12 col-xs-12 startExploringBanner">
        </div>
    </div>
</div>

<div id="comeOnDown" class="landingSection">
    <div class="container">
        <div class="col-md-12 col-xs-12">
            <h2>Come on Down</h2>
            <p>So you believe it: the sounds, the food, the magic—New Orleans is something to experience. But you want to see it for yourself. Can’t blame you. And it works out for us—because we’d love to see you too. </p>
            <p>
            To schedule a tour or to register for on-campus events, go on over here. And if you need a place to stay, talk to our friends at these hotels, and make sure to tell them we sent you (so you get those Big Easy friend-prices).
            </p>
        </div>
    </div>
</div>

<div id="landingbottom">
    <div class="container">

    <div class="landbox col-md-6">
        <ul>
            <li><a href="http://apply.loyno.edu/visit-loyola">Campus Tour or Event Registration</a>
            </li>
            <li><a href="http://www.campustravel.com/university/loyno/visit_loyolano.html">Hotels</a>
            </li>
        </ul>
    </div>
    </div>
</div>

<?php 
    include( 'includes/footer.php'); 
    include( 'includes/more-menu.php'); 
    include( 'includes/javascript.php'); 
?>

</body>
</html>