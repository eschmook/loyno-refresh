<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Transportation &amp; Safety | Loyola University New Orleans</title>
    <link type="text/css" rel="stylesheet" href="http://fast.fonts.net/cssapi/d5e07912-c037-4030-b268-eb9fd671dc66.css" />
    <!-- Bootstrap -->
    <link href="./css/bootstrap.css" rel="stylesheet">
    <!--<link rel="stylesheet" type="text/css" href="styles.css"> -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="./apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="./apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="./apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="./apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="./apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="./apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="./apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="./apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="./apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="./android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="./favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="./favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="./favicon-16x16.png">
    <link rel="manifest" href="./manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="./ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- rich preview meta -->
    <meta name="description" content="Our Campus page of Loyola University New Orleans" />
    <meta property="og:title" content="Loyola University New Orleans Home Page" />
    <meta property="og:description" content="Loyola University New Orleans is the best." />
    <meta property="og:image" content="./img/loyno_havoc.jpg" />

    <link rel="stylesheet" type="text/css" href="./fonts/faith-and-glory-one/faith-and-glory-one.css">
    <link rel="stylesheet" type="text/css" href="./fonts/faith-and-glory-two/faith-and-glory-two.css">
    <link rel="stylesheet" href="./css/flickity.css" media="screen">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <?php require('includes/header.php'); ?>

    <div id="landing-transportSafety" class="landingSection hero">
        <div class="container">
            <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
            <h2>Transportation &amp; Safety </h2>
                <p>
                <strong>New Orleans has over 70 distinct neighborhoods. You’re living in one of the best.</strong> </p>
                <p>Loyola is on the iconic St. Charles streetcar line and across the street from Audubon Park and Audubon Zoo. It’s a beautiful walk — or streetcar ride — through the oak trees to some of the best food and music in the city. And it’s just a 20-minute drive to the Central Business District and historic French Quarter. </p>

                <a href="http://apply.loyno.edu/sites/apply.loyno.edu/files/file_attach/campus-map-april-2016.pdf" class="more">download map</a>

                <a href="#transportationPolicy" class="slowscrolling "><img src="./img/svg/arrow-down.svg" alt="arrow to read more" class="arrow_more pulse_animation" /></a>

            </div>
  
        </div>
    </div>

  <div id="transportationPolicy" class="landingSection">
        <div class="container items">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h4 class="red">Transportation &amp; Safety </h4>
                <h5>Parking</h5>
                <p>
                Bring your car. Park on campus. Loyola offers parking at multiple street-level and garage locations around campus. To use these spaces, students must purchase a parking permit from Parking Services, located in Biever Hall. <a href="tel:5048653000" class="red">Call Parking Services at (504) 865-3000 for more information.</a></p>

                <hr class="gold" />

                <h5>Shuttle Service</h5>
                <p>Loyola offers shuttle service between the main campus and Broadway campus during the academic year. <a href="http://finance.loyno.edu/police/shuttle-services" class="red">Learn more</a></p>
                <p>
                We also share a Saturday Grocery &amp; Entertainment Shuttle with Tulane University. <a href="http://finance.loyno.edu/police/shuttle-services" class="red">Pick-up locations and times.</a></p>

                <hr class="gold" />

                <h5>Late-night shuttle service</h5>
                <p>Available through Gold Zone Transportation. Free, safe rides home at night from 6 p.m. to 3 a.m. The Gold Zone shuttle runs seven days a week and will take students between campus residences and campus buildings and will even provide pick-up from off-campus locations. <a href="tel:5043147233" class="red">For service, call (504) 314-SAFE</a>.

            </div>
        </div>
</div>

 <div id="safety" class="landingSection">
        <div class="container items">
            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
            <h2>Safety</h2>
            <p>
            <strong>Loyola University New Orleans’ Public Safety Department provides a safe environment to support your on-campus life.</strong> </p>

            <ul>
              <li><strong>Police are on campus.</strong> 
              University police officers are fully commissioned by the State of Louisiana and meet all required training and standards set forth by the state.</li>
              <li> <strong> Safety training courses. </strong> The Rape Aggression Defense System (RADS), a nationally recognized self-defense program for women, is taught regularly on campus.</li>
            </ul>

                <div id="accordion" role="tablist" aria-multiselectable="true" class="hidden">
                <p>
                Top 10 reasons to live with the Pack</p>

                  <div class="card">
                    <div class="card-header collapsed" role="tab" data-toggle="collapse" data-parent="#accordion" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne" id="headerOne">
                    1. Live within a community.
                    </div>

                    <div id="collapseOne" class="collapse card-block" role="tabpanel" aria-labelledby="headerOne">
                      <p>Living on campus makes it easy to meet new people, make lifelong friends, go to campus events, join student organizations, and participate in leadership experiences.</p>
                    </div>
                  </div><!-- card -->

                  <div class="card">
                    <div class="card-header collapsed" role="tab" data-toggle="collapse" data-parent="#accordion" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" id="headerTwo">2. Live and learn.
                    </div>

                    <div id="collapseTwo" class="collapse card-block" role="tabpanel" aria-labelledby="headerTwo">
                      <p>National research finds that students who live on campus have better grade point averages (GPAs) than students who live off campus. Students living on campus are also more satisfied with their college experience and more likely to graduate than their commuter counterparts.</p>
                    </div>
                  </div><!-- card -->

                  <div class="card">
                    <div class="card-header collapsed" role="tab" data-toggle="collapse" data-parent="#accordion" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree" id="headerThree">3. Live conveniently
                    </div>

                    <div id="collapseThree" class="collapse card-block" role="tabpanel" aria-labelledby="headerThree">
                     <p>Living on campus means no sitting in traffic or looking for parking. It means a stress-free commute to class and co-curricular activities. The library, classroom buildings, and on-campus dining options are less than a 10-minute walk from each residence hall.</p>
                    </div>
                  </div><!-- card -->

                  <div class="card">
                    <div class="card-header collapsed" role="tab" data-toggle="collapse" data-parent="#accordion" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour" id="headerFour">4. Live safely
                    </div>

                    <div id="collapseFour" class="collapse card-block" role="tabpanel" aria-labelledby="headerFour">
                     <p>Safety and security are our top priorities on campus. University Police, Residential Life staff, and on-call mental health counselors are available 24 hours a day, seven days a week.</p>
                    </div>
                  </div><!-- card -->

                  <div class="card">
                    <div class="card-header collapsed" role="tab" data-toggle="collapse" data-parent="#accordion" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive" id="headerFive">5. Live well
                    </div>

                    <div id="collapseFive" class="collapse card-block" role="tabpanel" aria-labelledby="headerFive">
                     <p>Students living on campus have convenient access to our University Sports Complex, group fitness classes, healthy dining options, worship services, and the Student Health Center.</p>
                    </div>
                  </div><!-- card -->

                  <div class="card">
                    <div class="card-header collapsed" role="tab" data-toggle="collapse" data-parent="#accordion" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix" id="headerSix">6. Live affordably
                    </div>

                    <div id="collapseSix" class="collapse card-block" role="tabpanel" aria-labelledby="headerSix">
                     <p>Living off campus may seem less expensive, but once rent, food, gas, water, electricity, internet/cable, furniture, and maintenance add up, living on campus often becomes not just cheaper but also much simpler — everything is included in one convenient bill.</p>
                    </div>
                  </div><!-- card -->

                  <div class="card">
                    <div class="card-header collapsed" role="tab" data-toggle="collapse" data-parent="#accordion" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven" id="headerSeven">7. Live in style
                    </div>

                    <div id="collapseSeven" class="collapse card-block" role="tabpanel" aria-labelledby="headerSeven">
                     <p>Living on campus offers a variety of options for students of all grade levels. First-year students may apply to live in a <a href="http://studentaffairs.loyno.edu/residential-life/themed-living-communities">Themed Living Community</a>: a group of students intentionally brought together by a common purpose in order to make living on campus a more personalized experience. We also offer a First Year Living environment to freshmen, which provides specialized events that assist first-year students in adjusting to college life. Sophomores, juniors, and seniors have the option to apply for suite- and apartment-style living, which offers additional common space and kitchen access. All of these opportunities enhance and complement your in-classroom experience — it all depends on what’s best for you.</p>
                    </div>
                  </div><!-- card -->
                  <div class="card">
                    <div class="card-header collapsed" role="tab" data-toggle="collapse" data-parent="#accordion" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight" id="headerEight">8. Live with support
                    </div>

                    <div id="collapseEight" class="collapse card-block" role="tabpanel" aria-labelledby="headerEight">
                     <p>As you adjust to life away from home, trained student and professional staff members in Residential Life are available to assist with personal and academic concerns 24/7. Additionally, resident ministers live in each building to interact and support students personally, academically, and spiritually.</p>
                    </div>
                  </div><!-- card -->
                  <div class="card">
                    <div class="card-header collapsed" role="tab" data-toggle="collapse" data-parent="#accordion" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine" id="headerNine">9. Live fully
                    </div>

                    <div id="collapseNine" class="collapse card-block" role="tabpanel" aria-labelledby="headerNine">
                     <p>Eating great food is a huge part of living in New Orleans, and all students are required to participate in a campus dining plan. Sodexo’s “dining partner” concept allows you to choose from over 25 dining options between Loyola’s campus and Tulane’s, including Starbucks and our new addition, locally owned Deaux pizzeria.</p>
                    </div>
                  </div><!-- card -->
                  <div class="card">
                    <div class="card-header collapsed" role="tab" data-toggle="collapse" data-parent="#accordion" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen" id="headerTen">10. Live Uptown
                    </div>

                    <div id="collapseTen" class="collapse card-block" role="tabpanel" aria-labelledby="headerTen">
                     <p>LOCATION, LOCATION, LOCATION! Living on campus puts you in the heart of the Uptown New Orleans area. Enjoy convenient access to the streetcar, Audubon Park, Audubon Zoo, and countless local restaurants and festivals. Living here is really a one-of-a-kind experience!</p>
                    </div>
                  </div><!-- card -->

                </div><!-- accordion -->
            </div>
    </div>
</div>

<?php 
    include( 'includes/footer.php'); 
    include( 'includes/more-menu.php'); 
    include( 'includes/javascript.php'); 
?>

</body>
</html>