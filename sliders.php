<?php

$sliderImage[] = "HomeSlide_01.png";
$sliderImageMobile[] = "2HomeSlide_crop_01.png";
$sliderDesc[] = "Our students are passionate, entrepreneurial, and driven.";
$sliderURL[] = "/academics.php";

$sliderImage[] = "HomeSlide_02.png";
$sliderImageMobile[] = "2HomeSlide_crop_02.png";
$sliderDesc[] = "In a city built for an innovator like you.";
$sliderURL[] = "/neworleans.php";

$sliderImage[] = "HomeSlide_03.png";
$sliderImageMobile[] = "2HomeSlide_crop_03.png";
$sliderDesc[] = "Start writing your story today.";
$sliderURL[] = "/admissions.php";

$sliderImage[] = "2017-07-28_topic-04_feast-st-ignatius-loyola-university-new-orleans.png";
$sliderImageMobile[] = "22017-07-28_topic-04_MOBILE_feast-st-ignatius-loyola-university-new-orleans.png";
$sliderDesc[] = "A day to reflect on St. Ignatius, whose life and spirituality shapes Loyola University.";
$sliderURL[] = "https://goo.gl/Ao5b2G";

$sliderImage[] = "2017-07-28_topic-01_summer-loyno-loyola-university-new-orleans.png";
$sliderImageMobile[] = "22017-07-28_topic-01_MOBILE_summer-loyno-loyola-university-new-orleans.png";
$sliderDesc[] = "The Summer Issue of loyno has arrived! READ IT HERE";
$sliderURL[] = "https://goo.gl/VYCKhZ";

$sliderImage[] = "2017-07-28_topic-03_max-well-loyola-university-new-orleans.png";
$sliderImageMobile[] = "22017-07-28_topic-03_MOBILE_max-well-loyola-university-new-orleans.png";
$sliderDesc[] = "New cafe by alumnus Maxwell Eaton to bring healthy and organic options to the community.";
$sliderURL[] = "https://goo.gl/FcyXpK";

$sliderImage[] = "2017-07-28_topic-02_general-news-loyola-university-new-orleans.png";
$sliderImageMobile[] = "22017-07-28_topic-02_MOBILE_general-news-loyola-university-new-orleans.png";
$sliderDesc[] = "Visit our newsroom to catch up on what’s been going on in the Loyola community!";
$sliderURL[] = "https://goo.gl/JBgqud";

?>
