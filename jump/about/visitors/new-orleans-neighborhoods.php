<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>New Orleans Neighborhoods - Loyola University New Orleans</title>
    <link type="text/css" rel="stylesheet" href="http://fast.fonts.net/cssapi/d5e07912-c037-4030-b268-eb9fd671dc66.css" />
    <!-- Bootstrap -->
    <link href="/css/bootstrap.css" rel="stylesheet">
    <!--<link rel="stylesheet" type="text/css" href="styles.css"> -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="./ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- rich preview meta -->
    <meta name="description" content="Home page of Loyola University New Orleans" />
    <meta property="og:title" content="Loyola University New Orleans Home Pagew" />
    <meta property="og:description" content="Loyola University New Orleans is the best." />
    <meta property="og:image" content="./img/loyno_havoc.jpg" />

    <link rel="stylesheet" type="text/css" href="/fonts/faith-and-glory-one/faith-and-glory-one.css">
    <link rel="stylesheet" type="text/css" href="/fonts/faith-and-glory-two/faith-and-glory-two.css">
    <link rel="stylesheet" href="/css/flickity.css" media="screen">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <?php require('../../../includes/header.php'); ?>

<div id="body-wrap-lp">
    <div id="landingtop">
        <div class="container">
            <div class="landbox">
                <ul>
                    <li><a href="map-loyola.php">Loyola Campus Map</a>
                    </li>
                    <li><a href="map-new-orleans.php">New Orleans Map + Driving Directions</a>
                    </li>
                    <li><a href="new-orleans-neighborhoods.php">New Orleans Neighborhoods</a>
                    </li>
                    <li><a href="/neworleans.php">More about New Orleans</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="landingSection">
        <div class="container">
            <div class="col-md-12">
                <h2>New Orleans Neighborhoods</h2>
                <h4>Lakeside, Riverside, Uptown, Downtown</h4>
                <p class="intro">
                    With a little inside info, finding your way around New Orleans is a cinch. First thing: north, south, east and west are virtually unknown here. We navigate by the landscape. North toward the lake and the Causeway is lakeside, south to the river is riverside, west is uptown, and east is downtown.</p>
                <p>
                    Second, we travel by neighborhoods, driving over to Carrollton or into the Faubourg Marigny. The National Register of Historic Places has designated 17 National Historic Districts in New Orleans. Two of them&mdash;the French Quarter and Garden District&mdash;are also National Historic Landmarks. (So is the St. Charles Avenue streetcar line.)</p>
                    
                <h4>Essential New Orleans</h4>
                <p>
                    New Orleans began, as most first time visitors here do, with the Vieux Carre&mdash;a square hugging an elbow curve in the Mississippi River. The architecture is European and Creole, the colors are Caribbean and the festive lifestyle is a New Orleans trademark.</p>
                <p>
                    For 300 years, the <strong>French Quarter</strong> has been the heart of the city, centered around Jackson Square. It is home to the French Market, Riverwalk, the Aquarium of the Americas, Spanish Plaza, museums, restaurants, and enough activity for a lifetime of sightseeing.</p>
                <p>
                    While the French Quarter, or Vieux Carre, is the most famous, there are many other fascinating neighborhoods to explore. Here's the scoop on just a few. Just downriver (to the east, if you must have compass point) is the first suburb, the Faubourg <strong>Marigny</strong> (Esplanade Avenue to Press Street, the river to St. Claude), dating from the early 18th century.</p>
                <p>
                    Brightly (sometimes wildly) painted Creole cottages cluster in the narrow streets dotted with churches, old warehouses, chic cafes, eateries and watering holes. Washington Square is the oasis; Frenchmen Street is the center of the club activity, and music drifts through the air along with the scent of the river and night-blooming jasmine.</p>
                <p>
                    Head toward the lake (north) along lovely Esplanade Avenue, at the back of the Quarter through <strong>Esplanade Ridge</strong> (North Rampart to City Park banking Esplanade Avenue). Considered part of <strong>Mid-City</strong> by locals, lovely mansions and shotgun and Creole cottages dominate the landscape scattered with occasional monuments. It's home to the Fair Grounds Race Course by Churchill Downs, and ends at lovely Bayou St. John and City Park &mdash; home to the New Orleans Museum of Art, Bestoff Sculpture Garden, and 1,500 acres of botanical gardens, sports, theme parks and children's activities.</p>
                <p>
                    Toward Uptown, west of the French Quarter, across Canal Street, is the<strong> Central Business District</strong> (Canal Street to the Pontchartrain Expressway, the river to I-10). This is where the Americans first settled when the Creoles wanted to keep the French Quarter to themselves. A mix of office and industrial buildings centered around Lafayette Square, this area in the last decade has seen its revitalization as a flourishing arts and residential district. Wonderful architectural conversions have created a neighborhood of galleries, restaurants and studios like the Preservation Resource Center, the Contemporary Arts Center and the acclaimed New Orleans School of GlassWorks & Printmaking Studio. </p>
                <p><strong>The Garden District</strong>: its two sections, Lower Garden and the Garden District, were both subdivided from plantations. The Lower Garden District (I-10 to Jackson Avenue, St. Charles to Tchoupitoulas) is more cottage than mansion. Named for its origins, lower on the social and style scale than the Garden District, it is an architectural buffet of Greek Revival, Victorian, and historic cottage styles. A few sophisticated historic townhouses gather around Coliseum Square, with its antique ambiance. Classical street names take on local pronunciations: Melpomene (mel-puh-meen), Calliope (kal-ee-ope).</p>

                <p>
                    Trendy shops along <strong>Magazine Street</strong> (from the French magasin , meaning"shop") and the hip, laid-back style of the area have inspired many to dub it one of the coolest neighborhoods in the U.S., on par with Greenwich Village.Garden District (Jackson Avenue to Louisiana Avenue, St. Charles Avenue to Magazine Street): Americans unwelcome in the French Quarter showed off their wealth by building huge mansions with the lavish gardens for which the Garden District is renowned. Every house tells a tale, as the novels of Anne Rice remind us.</p>
                <p>
                    Home to Loyola, the boundaries of the <strong>Uptown</strong> historic district extend from Louisiana Avenue to Lowerline Street and from Tchoupitoulas to Claiborne. Adjacent is the<strong> Carrollton</strong> area, centered around Carrollton Avenue, where the river bends. Uptown is heart and home to residents, chic boutiques and a multitude of eateries and outdoor cafes where the sound of screen doors shutting and oyster shells opening is music to the patrons' ears. This largest residential enclave of the city is known for its polished wooden floors and high ceilings with lazy fans. The architecture is diverse; the landscape is green with live oaks; and the air perfumed by sweet olive, magnolias and night-blooming jasmine. </p>
                <!-- InstanceEndEditable -->
            </div>


            </div>
        </div>
        <!-- container -->

    </div>
    <!-- #body-wrap -->

<?php 
    include( '../../../includes/footer.php'); 
    include( '../../../includes/more-menu.php'); 
    include( '../../../includes/javascript.php'); 
?>

</body>
</html>