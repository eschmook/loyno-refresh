<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>New Orleans Map and Driving Directions - Loyola University New Orleans</title>
    <link type="text/css" rel="stylesheet" href="http://fast.fonts.net/cssapi/d5e07912-c037-4030-b268-eb9fd671dc66.css" />
    <!-- Bootstrap -->
    <link href="/css/bootstrap.css" rel="stylesheet">
    <!--<link rel="stylesheet" type="text/css" href="styles.css"> -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="./ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- rich preview meta -->
    <meta name="description" content="Home page of Loyola University New Orleans" />
    <meta property="og:title" content="Loyola University New Orleans Home Pagew" />
    <meta property="og:description" content="Loyola University New Orleans is the best." />
    <meta property="og:image" content="./img/loyno_havoc.jpg" />

    <link rel="stylesheet" type="text/css" href="/fonts/faith-and-glory-one/faith-and-glory-one.css">
    <link rel="stylesheet" type="text/css" href="/fonts/faith-and-glory-two/faith-and-glory-two.css">
    <link rel="stylesheet" href="/css/flickity.css" media="screen">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <?php require('../../../includes/header.php'); ?>

<div id="body-wrap-lp">
    <div id="landingtop">
        <div class="container">
            <div class="landbox">
                <ul>
                    <li><a href="map-loyola.php">Loyola Campus Map</a>
                    </li>
                    <li><a href="map-new-orleans.php">New Orleans Map + Driving Directions</a>
                    </li>
                    <li><a href="new-orleans-neighborhoods.php">New Orleans Neighborhoods</a>
                    </li>
                    <li><a href="/neworleans.php">More about New Orleans</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="landingSection">
        <div class="container">

            <h2>City of New Orleans Map </h2>
            <img src="images/loyno_city_map.png" />
            <br /><br /><br />
            <h3>Driving Directions</h3>
            <p><strong>Directions to Loyola from I-10 East (Slidell) Traveling West:</strong>
                <br />
            </p>
            <ol>
                <li>Follow I-10 W to US-90 W / S Claiborne Ave. Using the middle lane, take the U.S. 90 business
                    <br /> W/ S Claiborne Ave exit from I-10 W.</li>
                <li>Head south and merge onto S Claiborne Ave. (1.1 m)</li>
                <li>Turn left onto Napoleon Ave. (0.6 m)</li>
                <li>Turn right onto Loyola Ave. (1.0 m)</li>
                <li>Turn right onto Palmer Ave. (0.1 m)</li>
                <li>Turn left onto LaSalle Place. Remaining on LaSalle, cross Calhoun Street into Loyola&rsquo;s campus.
                    <br /> (0.1 m)</li>
                <li>Turn right onto East Road, then turn left onto North Road, then turn left onto West Road.</li>
                <li>The West Road Parking Garage will be straight ahead.</li>
            </ol>
            <p> <strong>Directions to Loyola from the West Bank Traveling East:</strong>
                <br />
            </p>
            <ol>
                <li>Follow US-90 BUS E. Take the exit toward US-90 W/Claiborne Ave/Earhart Blvd from US-90
                    <br /> BUS E</li>
                <li> Merge onto Earhart Blvd. (0.3 m)</li>
                <li> Turn left onto S Claiborne Ave. (1.3 m)</li>
                <li> Turn left onto Napoleon Ave. (0.6 m)</li>
                <li> Turn right onto Loyola Ave. (1.0 m)</li>
                <li> Turn right onto Palmer Ave. (0.1 m)</li>
                <li> Turn left onto LaSalle Place. Remaining on LaSalle, cross Calhoun Street into Loyola&rsquo;s campus.
                    <br /> (0.1 m)</li>
                <li> Turn right onto East Road, then turn left onto North Road, then turn left onto West Road.</li>
                <li>The West Road Parking Garage will be straight ahead.</li>
            </ol>
            <p><strong>Directions to Loyola from I-10 West (Baton Rouge) Traveling East:</strong>
                <br />
            </p>
            <ol>
                <li>Head east on I-10 E.</li>
                <li> Keep right at the fork to stay on I-10 E, follow signs for New Orleans Business District/Interstate
                    <br /> 10 E. (1.6 m)</li>
                <li> Take exit 232 toward Airline Dr/Tulane Ave/Carrollton Avenue/LA-61 (0.5 m)</li>
                <li> Keep left at the fork, follow signs for Carrollton Avenue and merge onto S Carrollton Ave. (2.6 m)</li>
                <li> Turn left onto St. Charles Ave. (1.0 m)</li>
                <li> Turn left onto West Rd. (0.2 m)</li>
                <li> The West Road Parking Garage will be straight ahead.</li>
            </ol>

        </div>
    </div>
</div>
<!-- #body-wrap -->
<?php 
    include( '../../../includes/footer.php'); 
    include( '../../../includes/more-menu.php'); 
    include( '../../../includes/javascript.php'); 
?>

</body>
</html>