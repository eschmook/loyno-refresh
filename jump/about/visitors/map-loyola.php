<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Campus Map - Loyola University New Orleans</title>
    <link type="text/css" rel="stylesheet" href="http://fast.fonts.net/cssapi/d5e07912-c037-4030-b268-eb9fd671dc66.css" />
    <!-- Bootstrap -->
    <link href="/css/bootstrap.css" rel="stylesheet">
    <!--<link rel="stylesheet" type="text/css" href="styles.css"> -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="./ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- rich preview meta -->
    <meta name="description" content="Home page of Loyola University New Orleans" />
    <meta property="og:title" content="Loyola University New Orleans Home Pagew" />
    <meta property="og:description" content="Loyola University New Orleans is the best." />
    <meta property="og:image" content="./img/loyno_havoc.jpg" />

    <link rel="stylesheet" type="text/css" href="/fonts/faith-and-glory-one/faith-and-glory-one.css">
    <link rel="stylesheet" type="text/css" href="/fonts/faith-and-glory-two/faith-and-glory-two.css">
    <link rel="stylesheet" href="/css/flickity.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/css/campus-map.css" media="screen" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <?php require('../../../includes/header.php'); ?>

<div id="body-wrap-lp">
    <div id="landingtop">
        <div class="container">
            <div class="landbox">
                <ul>
                    <li><a href="map-loyola.php">Loyola Campus Map</a>
                    </li>
                    <li><a href="map-new-orleans.php">New Orleans Map + Driving Directions</a>
                    </li>
                    <li><a href="new-orleans-neighborhoods.php">New Orleans Neighborhoods</a>
                    </li>
                    <li><a href="/neworleans.php">More about New Orleans</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="landingSection">
        <div class="container">

            <h2>Maps + Directions</h2>
            <!-- campus map -->
<?php
// This script is used on both Admissions' campus map and mobile map. 
// If this is changed test those two maps.

//ARRAY OF ALL CAMPUS LOCATIONS
//STRUCURE =  Location, Latitude, Longitude

$locations = array( 
  array("Main Campus", 29.934714, -90.12144),
  array("Broadway Campus", 29.936769, -90.12814),
  array("Academic Quad", 29.935 , -90.12126),
  array("Biever Hall", 29.937, -90.12064),
  array("Bobet Hall", 29.93526, -90.12116),
  array("Broadway Activities Center", 29.93629, -90.1279),
  array("Buddig Hall", 29.93705 , -90.11999),
  array("Cabra Hall", 29.93693, -90.12745),
  array("Carrollton Hall", 29.93671, -90.11987),
  array("College of Law", 29.93692, -90.12857),
  array("Communications / Music Complex", 29.93378, -90.12086),
  array("Danna Student Center", 29.936, -90.12054),
  array("Greenville Hall", 29.93674, -90.12794),
  array("Monroe Library", 29.93545, -90.12006),
  array("Marquette Hall", 29.93472, -90.12145),
  array("Mercy Hall", 29.93712, -90.11889),
  array("Miller Hall", 29.93507, -90.12008),
  array("Monroe Hall", 29.93422, -90.1204),
  array("Most Holy Name of Jesus", 29.93454, -90.12201),
  array("Palm Court", 29.93435, -90.12096),
  array("Peace Quad", 29.9357, -90.12085),
  array("Residential Quad", 29.93677, -90.1203),
  array("St. Mary's Hall/Visual Arts", 29.93577, -90.1283),
  array("Stallings Hall", 29.93515, -90.12164),
  array("Thomas Hall", 29.93404, -90.1214),
  array("University Police", 29.93681, -90.12077),
  array("University Sports Complex", 29.93754, -90.11978)         
); 


if($_POST['building'] != "" ) {
  foreach($locations as $local) {
    if($_POST['building'] == str_replace("'", '', $local[0])) {
      $lat = $local[1];
      $lng = $local[2];
      echo $lat."|".$lng;  // value passed to js
    }
  }
} else {
  $lat = "29.934714";
  $lng = "-90.12144";
}
//echo $lat."|".$lng;
?>
            <div id="campus-map">
                <ul class="options">
                    <li class="tab main-campus active"><a href="#">Main Campus</a>
                    </li>
                    <li class="tab broadway"><a href="#">Broadway Campus</a>
                    </li>
                    <li class="locations">
                        <select name="select-building" id="select-building">
                            <option value="" selected="selected">Please choose...</option>
                            <?php foreach($locations as $local) { ?>
                            <option value="<?php echo str_replace(" '", ' ', $local[0]); ?>"><?php echo $local[0]; ?></option> 
                  <?php } ?>
              </select> 
            </li>
          </ul>
          <div id="mapHolder">
            <div id="map"></div> 
          </div> <!-- #mapHolder -->
        </div> <!-- #campus-map -->
                
        <h2>Driving Directions</h2>
          <div id="campusmap-pdf-link"><a class="no-icon" href="http://apply.loyno.edu/sites/apply.loyno.edu/files/file_attach/campus-map-april-2016.pdf"><img height="195" width="249" src="images/campus-map-link.gif" alt="" /></a><br />
          <a href="http://apply.loyno.edu/sites/apply.loyno.edu/files/file_attach/campus-map-april-2016.pdf" target="_blank">Download Printable Version of the Map</a></div>

          <p><b>From I-10 West Traveling East</b>: Follow the signs toward the Central Business District. As you enter the downtown area, follow the signs that merge to Hwy. 90 Westbank/Superdome/Claiborne Ave. After merging onto Hwy. 90, get in the righthand lane and exit at St. Charles Ave./Carondelet St. (do not cross the bridge). At the second traffic light, turn right onto St. Charles Ave. Follow St. Charles Ave. for four miles. Loyola's main campus is on the right at 6363 St. Charles Ave. The Broadway campus is located on the left at 7214 St. Charles Ave.</p>
          <p><b>From I-10 East Traveling West:</b> As you enter the downtown area, follow the signs to Hwy. 90 Business/West Bank. Exit at St. Charles Ave./Carondelet Street (do not cross bridge). At the second traffic light, turn right onto St. Charles Ave. Follow St. Charles Ave. for four miles. Loyola's main campus is on the right at 6363 St. Charles Ave. The Broadway campus is located on the left at 7214 St. Charles Ave.</p>
          <p><i>Loyola University New Orleans is located at </i><a href="http://maps.google.com/maps?hl=en&amp;sugexp=ldymls&amp;xhr=t&amp;q=29.9341396,+-90.121721&amp;cp=22&amp;um=1&amp;ie=UTF-8&amp;sa=N&amp;tab=wl"><i>29&deg;56&deg;3N, -90&deg;7&deg;19W (29.9341396, -90.121721)</i></a>
          </p>
          <h2>Visitor Information</h2>
          <h5>Need a place to stay during your visit?</h5>
          <p> New Orleans is a popular tourist and convention destination. It is important to make your hotel reservations as early as possible. In an effort to assist you, we are pleased to recommend the following establishments for their good reputation and proximity to campus. <a href="http://www.campustravel.com/university/loyno/visit_loyolano.html">Book Your Hotel &raquo;</a>
          </p>

    </div>
  </div>

</div><!-- #body-wrap -->


<?php 
    include( '../../../includes/footer.php'); 
    include( '../../../includes/more-menu.php'); 
    include( '../../../includes/javascript.php'); 
?>
<script src="http://maps.google.com/maps?file=api&v=2&sensor=true&key=AIzaSyAoTXBmi6Dda9jriF-mwpfjwXdvhVasgP8" type="text/javascript"></script>
<script type="text/javascript" src="/js/campus-map.js"></script>


</body>
</html>