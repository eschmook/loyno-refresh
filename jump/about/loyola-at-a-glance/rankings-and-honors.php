<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Rankings + Honors - Loyola University New Orleans</title>
    <link type="text/css" rel="stylesheet" href="http://fast.fonts.net/cssapi/d5e07912-c037-4030-b268-eb9fd671dc66.css" />
    <!-- Bootstrap -->
    <link href="/css/bootstrap.css" rel="stylesheet">
    <!--<link rel="stylesheet" type="text/css" href="styles.css"> -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="./ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- rich preview meta -->
    <meta name="description" content="Rankings + Honors Loyola University New Orleans" />
    <meta property="og:title" content="Loyola University New Orleans Rankings + Honors" />
    <meta property="og:description" content="A leading Catholic, Jesuit university, Loyola offers students from all faith traditions a campus environment rich with both spirituality and academic inquiry." />
    <meta property="og:image" content="./img/loyno_havoc.jpg" />

    <link rel="stylesheet" type="text/css" href="/fonts/faith-and-glory-one/faith-and-glory-one.css">
    <link rel="stylesheet" type="text/css" href="/fonts/faith-and-glory-two/faith-and-glory-two.css">
    <link rel="stylesheet" href="/css/flickity.css" media="screen">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <?php require('../../../includes/header.php'); ?>

<div id="body-wrap-lp">
  <div id="landingtop">
      <div class="container">
        <div class="landbox">
            <ul>
                <li><a href="demographics.php">Demographics</a></li>
                <li><a href="rankings-and-honors.php">Rankings + Honors</a></li>
                <li><a href="loyola-history.php">Loyola History</a></li>
                <li><a href="jesuit-tradition.php">Jesuit Tradition</a></li>
            </ul>
        </div>
      </div>
  </div>

  <div class="landingSection">
    <div class="container"> 
        <h2>Rankings + Honors</h2>
          <h3>We rate great.</h3>
          
          <div class="honors">
            <div class="col-md-3">
              <img src="images/FiskeFeature.jpg" alt="" width="150" height="150" />
            </div>
            <div class="col-md-9">
              <p><strong>Named in Fiske Guide to Colleges 2018</strong><br/>For more than 30 years, millions of students, parents, and guidance counselors have relied on the Fiske Guide to Colleges to present the best and most interesting schools during their college search. <i>Fiske Guide to Colleges 2018</i> spotlights students' easy access to faculty; high level of collaborations, challenging academic climate, intimate classes, and dozens of comprehensive undergraduate degree programs.</p>
            </div>
          </div>
          
          <div class="honors">
            <div class="col-md-3">
              <img src="images/fulbright-logo.jpg" alt="" width="150" height="150" />
            </div>
            <div class="col-md-9">
              <p><strong>Top U.S. Fulbright Producer</strong><br/>The U.S. Department of State's Bureau of Educational and Cultural Affairs named Loyola University New Orleans among the Top U.S. Fulbright Producers for the 2015-2016 academic year. Recipients of Fulbright grants are selected on the basis of academic and professional achievement, as well as demonstrated leadership potential.</p>
            </div>
          </div>

          <div class="honors">
            <div class="col-md-3">
              <a href="http://www.usnews.com/education/online-education/loyola-university-new-orleans-ONUR0191/nursing"><img src="images/OnlinePrograms-GradNursing-2017.png" alt="" width="150" height="141" /></a>
            </div>
            <div class="col-md-9">
              <p><strong>U.S. News &amp; World Report 'Best Colleges'</strong></p>
              <p>#4 in the region for diversity<br />
                #10 &quot;Best Regional Universities of the South&quot;<br />
                #12 &quot;Best Value&quot; in the South<br />
              Top 150 online education programs<br/>
              #39 Best Online Nursing Programs</p>
            </div>
          </div>

          <div class="honors">
            <div class="col-md-3"></div>
            <div class="col-md-9">
            <p><strong>The Princeton Review's 'Best Colleges'</strong>
              </p>
              <p>#13 Lots of Race/Class Interaction<br />
                      #11 Town-Gown Relations are Great<br />
                      Best 381 Colleges <br />
                      Best 294 Business Schools</p>
            </div>
          </div>
          
          <div class="honors">
            <div class="col-md-3"></div>
            <div class="col-md-9">
            <p><strong>MONEY Magazine's &quot;Best Colleges for Your Money&quot;</strong>
              </p>
              
            </div>
          </div>

          <div class="honors">
            <div class="col-md-3">
              <img src="images/2017-best-value-masters-in-nurse-practitioner-programs.png" />
            </div>
            <div class="col-md-9">
              <p><strong>2017 Best Value for Masters in Nurse Practitioner Program</strong></p>
              <p>Loyola University New Orleans School of Nursing's Family Nurse Practitioner (FNP) online master's degree program ranks among the Top 30 in the U.S. and is a &quot;Best Value&quot; school. Loyola's Online Graduate Program in Nursing is designed to help nurses who want to become nurse practitioners, nursing managers, administrators and health care executives to advance their careers.</p>
            </div>
          </div>
          
          <div class="honors">
            <div class="col-md-3">
              <img src="images/Grad-School-Hub-Most-Affordable-Online-Programs-2017.png" />
            </div>
            <div class="col-md-9">
              <p><strong>Grad School Hub 2017 Most Affordable Online Programs</strong></p>
              <p>Loyola University New Orleans was ranked #24 among the best affordable online RN to MSN degree programs by Grad School Hub. The ranking considered accreditation, tuition, and other rankings and recognition.</p>
            </div>
          </div>
          
          
          <div class="honors">
            <div class="col-md-3">
              <img src="images/best-online-masters-in-nursing.jpg" />
            </div>
            <div class="col-md-9">
              <p><strong>College Choice 2017</strong></p>
              <p>College Choice ranked Loyola University New Orleans #46 in its Best Online Master's Degrees in Nursing. In 2016, College Choice ranked Loyola in the Top 50 Catholic Colleges and Universities, No. 11 overall among 2016 Best Regional Southern Universities, and #19 overall in Best Master's in Nursing Programs and #49 in Best Nurse Practitioner Programs.</p>
            </div>
          </div>

          <div class="honors">
            <div class="col-md-3">
              <img src="images/healthcare-management-badge.jpg" alt="" width="150" height="158" />
            </div>
            <div class="col-md-9">
              <p><strong>Healthcare Management Degree Guide 2016</strong></p>
              <p>The Healthcare Management Degree Guide ranked Loyola University New Orleans #19 in Best Master's Degree Programs in Healthcare Management for students who want to get the most &quot;bang for their buck.&quot; Programs were selected for ranking based on debt ratings, cost of attendance, amount of tuition assistance provided, accreditation, and acknowledgement by a national ranking body.</p>
            </div>
          </div>

          <div class="honors">
            <div class="col-md-3">
              <img src="images/college-raptor-hidden-gem.jpg" width="150" height="116" alt="College Raptor Hidden Gem 2015" />
            </div>
            <div class="col-md-9">
              <p><strong>College Raptor Hidden Gem 2015</strong></p>
              <p>College matching platform, College Raptor Inc., named Loyola University the top "hidden gem" for the state of Louisiana. The distinction is defined as high-caliber colleges and universities which receive fewer than 5,000 applicants per year but have a total enrollment of greater than 1,000.</p>
            </div>
          </div>

          <div class="honors">
            <div class="col-md-3">
              <img src="images/business-insider.jpg" />
            </div>
            <div class="col-md-9">
              <p><strong>Business Insider's Best College Campuses In America 2015</strong></p>
              <p>Loyola University New Orleans was ranked #13 among college campuses in America. The list combined rankings of more than 100 colleges for categories including college libraries, dorms, campus scenery, quality of life, and more.</p>
            </div>
          </div>  

          <div class="honors">
            <div class="col-md-3">
              <img src="images/presidents-honor-roll.jpg" alt="President's 2014 Higher Education Community Service Honor Roll" width="150" height="154" />
            </div>
            <div class="col-md-9">
              <p><strong>President's 2015 Higher Education Community Service Honor Roll</strong></p>
              <p>Loyola University New Orleans was named to the President's 2015 Higher Education Community Service Honor Roll, which highlights the role colleges and universities play in solving community challenges. As a result of these efforts, more students are likely to pursue a lifelong path of civic engagement that achieves meaningful and measurable outcomes in the communities they serve.</p>
            </div>
          </div>
                
          <h4>Other Rankings</h4>
            <ul>
              <li>College of Law&rsquo;s moot court program was ranked as a top 10 advocacy programs in the country by The National Jurist.</li>
              <li>Loyola&rsquo;s Wolf Pack Athletics Program has been named a prestigious <a href="http://www.loyolawolfpack.com/article/1273.php">NAIA Champions of Character Five Star Institution</a>, recognized for excellence in integrity, respect, sportsmanship and leadership.</li>
              <li>BestCollege.org ranks our Online Master of Science in Nursing degree program among the                                      nation&rsquo;s best</li>
              <li>Ranked #15 nationwide among master&rsquo;s universities for the number of graduates who go on to successfully receive doctoral degrees and # 35 nationwide for the number of alumni who join the Peace Corps by Washington Monthly.</li>
              <li>Ranked #34 for&nbsp;undergraduate participation in study abroad programs&nbsp;for those universities ranked as a Top 40 Master&rsquo;s Institution by the Carnegie Classification of Institutions of Higher Education by the Institute for International Education and the U.S. Department of State.</li>
            </ul>

      </div>
    </div>

</div><!-- #body-wrap -->

<?php 
    include( '../../../includes/footer.php'); 
    include( '../../../includes/more-menu.php'); 
    include( '../../../includes/javascript.php'); 
?>

</body>
</html>