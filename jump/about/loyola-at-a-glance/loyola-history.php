<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Loyola History - Loyola University New Orleans</title>
    <link type="text/css" rel="stylesheet" href="http://fast.fonts.net/cssapi/d5e07912-c037-4030-b268-eb9fd671dc66.css" />
    <!-- Bootstrap -->
    <link href="/css/bootstrap.css" rel="stylesheet">
    <!--<link rel="stylesheet" type="text/css" href="styles.css"> -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="./ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- rich preview meta -->
    <meta name="description" content="History of Loyola University New Orleans" />
    <meta property="og:title" content="Loyola University New Orleans History" />
    <meta property="og:description" content="A leading Catholic, Jesuit university, Loyola offers students from all faith traditions a campus environment rich with both spirituality and academic inquiry." />
    <meta property="og:image" content="./img/loyno_havoc.jpg" />

    <link rel="stylesheet" type="text/css" href="/fonts/faith-and-glory-one/faith-and-glory-one.css">
    <link rel="stylesheet" type="text/css" href="/fonts/faith-and-glory-two/faith-and-glory-two.css">
    <link rel="stylesheet" href="/css/flickity.css" media="screen">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

   <?php require('../../../includes/header.php'); ?>

<div id="body-wrap-lp">
  <div id="landingtop">
      <div class="container">
        <div class="landbox">
            <ul>
                <li><a href="demographics.php">Demographics</a></li>
                <li><a href="rankings-and-honors.php">Rankings + Honors</a></li>
                <li><a href="loyola-history.php">Loyola History</a></li>
                <li><a href="jesuit-tradition.php">Jesuit Tradition</a></li>
            </ul>
        </div>
      </div>
  </div>
  <div class="landingSection">
    <div class="container">
      <div class="col-md-12"> 
      	<h2>Loyola History</h2>
          <h3>The Arrival of the Jesuits</h3>
          <p>The Jesuits were among the earliest settlers of New Orleans and Louisiana. A Jesuit chaplain accompanied Iberville on his second expedition, and the fathers are credited with introducing the growing of sugar cane to Louisiana, paving the way for one of the state&rsquo;s prime industries. They probably brought this from their West Indies farms and planted it on the plantation they bought from former Governor Bienville in 1725. This tract, used by the fathers as a staging area or supply base for their activities in ministering to the needs of settlers and Indians in the up-country, was located &quot;across the common&quot; (now Canal Street), running along the Mississippi River to what is now Jackson Avenue. When the Jesuit order was banned from the French colonies in 1763, the land was sold at public auction.</p>
          <p>The city&rsquo;s leaders, including Bienville, had long hoped for a Jesuit college. After the Jesuit order was restored, the Bishop of New Orleans implored the Jesuits in France to come to the city. In 1837, seven Jesuit priests arrived. After weighing several sites, they decided that Grand Coteau, in St. Landry Parish, was a better site for their boarding college than the fever-ridden city.</p>
          <h3>Establishment of New Orleans' first Jesuit college</h3>
          <p>Meanwhile, New Orleans continued its dramatic growth, despite yellow fever. The desire for a Jesuit college here intensified in both the citizens and the fathers. In 1847, the priests bought a small piece of the same land they had owned nearly a century before, and in 1849, the College of the Immaculate Conception opened its doors at the corner of Baronne and Common streets.</p>
          <p>This college became a well-established and beloved institution. As the city grew, however, it became obvious to Rev. John O&rsquo;Shanahan, S.J., superior general of the province, that the downtown area would become too congested for a college. He began looking for a suburban site.</p>
          <p>The Cotton Centennial Exposition in 1884 had given impetus to the development of the uptown section of the city, especially around Audubon Park. This area was reached by the New Orleans and Carrollton Railroad which ran from Lee Circle to the City of Carrollton on the present roadbed of the St. Charles streetcar line. Father O&rsquo;Shanahan learned that a large site directly across from the park was available. This was the site of the Foucher Plantation, owned by Paul Foucher, son of a New Orleans mayor and son-in-law of Etienne de Bore, famed as the granulator of sugar from cane syrup.</p>
          <p>The entire Foucher site was offered to Father O&rsquo;Shanahan for the sum of $75,500. It included the land now occupied by Loyola and Tulane universities, and Audubon Place. The priest&rsquo;s advisers dissuaded him from purchasing this lest the acquisition of such a large tract bring on the charge of commercialism. He acceded, but said later he wished he had not since he could have within 10 days sold enough of the property &quot;to pay for the entire tract I bought and to put aside a sinking fund for the education of our young men.&quot;</p>
          <p>The section of the Foucher estate Father O&rsquo;Shanahan bought in 1886 fronted on St. Charles and ran approximately to the Claiborne canal. It was purchased with the assistance of Chief Justice Edward Douglass White, a Jesuit alumnus, and the Brousseau family. The price was $22,500, paid in three installments at six percent interest. On the day the act was signed, the fathers were offered $7,500 more for the property.</p>
          <p>In May 1890, the parish of Most Holy Name of Jesus was established for the area. Rev. John Downey, S.J., was the first pastor. A frame church, known affectionately among Orleanians as &quot;Little Jesuits,&quot; was built, and Mass was celebrated in it in May 1892.</p>
          <h3>Loyola College opens</h3>
          <p>In 1904, the long-planned Loyola College, together with a preparatory academy, opened its doors. First classes were held in a residence located to the rear of the church on what is now Marquette Place. The first president was the Rev. Albert Biever, S.J., who was appointed by the provincial, Rev. William Power, S.J.</p>
          <p>The college grew steadily. Father Biever promised and did give a holiday when the student body reached 50. In 1907, Father Biever called a meeting of prominent Catholic laymen to plan for a new building. Acting chairman was W.E. Claiborne. Out of his group grew the Marquette Association for Higher Education with B.A. Oxnard as chairman. In 1910, this group, with the assistance of its ladies auxiliary, was responsible for the building of Marquette Hall, queen of Loyola&rsquo;s buildings and centerpiece of its campus horseshoe. Strongly encouraged by Archbishop Blenk and prominent New Orleanians, the Jesuits and the Marquette Association had several years previously begun to make plans for expansion to a university.</p>
          <p>In 1911, the Jesuit schools in New Orleans were reorganized. Immaculate Conception College became exclusively a college preparatory school and was given the preparatory students of Loyola College. The downtown institution relinquished its higher departments&mdash;what are now known as college programs&mdash;to Loyola, which was in the process of becoming a university.</p>
          <h3>Becoming a university</h3>
          <p>On May 28, 1912, a bill was introduced in the Louisiana Senate by Senator William H. Byrnes, Jr., of Orleans Parish which proposed to grant a university charter to Loyola. It was passed unanimously and sent to the State House of Representatives. There was some backstage opposition, and Father Biever, fearing a fatal snag, made an impassioned speech to the house. The bill passed, and on July 10, 1912, the governor signed the act authorizing Loyola to grant university degrees.</p>
          <p>Under the direction of the dynamic Father Biever and with the advice and financial support of New Orleans citizens, the new university grew dramatically. Thomas Hall, residence for the fathers, was dedicated in 1912. The new church known as the McDermott Memorial, with its soaring tower, arose in 1913.</p>
          <p>In that year also the New Orleans College of Pharmacy, incorporated in 1900 by its founder, Dr. Philip Asher, chose to affiliate with Loyola. In 1919, the college merged completely with the university. The college was discontinued in 1965.</p>
          <p>The School of Dentistry was organized in 1914 with Dr. C. Victor Vignes as first dean. First classes were held in Marquette Hall. The school was transferred to Bobet Hall when that building was completed in 1924. The college was phased out between 1968 and 1971.</p>
          <p>The School of Law also was established in 1914 with Judge John St. Paul as founding dean. First classes were held at night in Alumni Hall near the College of Immaculate Conception. However, after the first year they were moved to the new university.</p>
          <p>Dr. Ernest Schuyten had founded the New Orleans Conservatory of Music and Dramatic Art in 1919. It was first located at Felicity and Coliseum streets and later moved to Jackson Avenue and Carondelet Street. It was incorporated into Loyola University in 1932 as the College of Music. The next year it moved to the Loyola campus with Dr. Schuyten as dean.</p>
          <p>The roots of educating adult students date back to 1919 when evening courses were first offered at Loyola for students who were unable to pursue full-time degree programs. By 1949, the demand for such evening courses had grown to an extent that the university decided to establish an Evening Division to serve the educational needs of working adults. In 1970, the Evening Division, with an enrollment of 1,200 students, was chartered as City College, with its own full-time faculty. In 2006, the university made each college responsible for educating undergraduate adult students. City College was discontinued as an administrative unit and its faculty became department members in the other colleges.</p>
          <p>From 1926 to 1947, a four-year degree program leading to a bachelor of science degree in economics was offered by the College of Arts and Sciences. In 1947, the Department of Commerce of the College of Arts and Sciences expanded into the full-fledged College of Business Administration granting a bachelor of business administration degree. The college moved into Stallings Hall shortly thereafter. Dr. John V. Conner was the first dean. In 1950, the college was admitted to associate membership in the Association to Advance Collegiate Schools of Business, and in 1957, the college was admitted to full membership. In 1983, the college was renamed the Joseph A. Butt, S.J., College of Business Administration in honor of the Jesuit priest who taught generations of Loyola business students. The college moved to Miller Hall, its present home, in 1986.</p>
          <h3>Educating the whole person</h3>
          <p>The university thus has a colorful and distinguished history marked by the zeal and scholarship of the Jesuit fathers and the valued advice and support of leading citizens of New Orleans. Hundreds of the city&rsquo;s top leaders received their education from the Jesuits at Loyola University, or its predecessor, the College of the Immaculate Conception. Teachers, scientists, attorneys, pharmacists, musicians, and business executives call Loyola their alma mater.</p>
          <p>Loyola has a colorful sports history. A double-decker stadium on Freret Street was the scene of exciting football games, including the first collegiate night game in the south. Olympic and national champions have worn the maroon and gold. In 1945 the basketball team won the National Intercollegiate Basketball Championship Tournament. The intercollegiate athletics program was discontinued in 1972 but reinstated in 1991, following a student referendum in which students voted for its return. The Wolfpack currently competes in the N.A.I.A. (National Association of Intercollegiate Conference) for both men and women.</p>
          <h3>An expanding campus</h3>
          <p>In 1964, Loyola completed major physical plant expansion with the dedication of three new buildings, a 404-student residence hall, a university center, and a central heating/cooling plant. In 1967, Buddig Hall, a 412-student women&rsquo;s residence, was dedicated.</p>
          <p>In 1969, the university completed the largest academic structure in its history, the 180,000-square-foot J. Edgar Monroe Memorial Science Building. Today this impressive structure houses science-oriented departments and is known as Monroe Hall.</p>
          <p>In 1984, the university purchased the 4.2-acre Broadway campus, formerly the campus of St. Mary&rsquo;s Dominican College. The Broadway campus, located on St. Charles Avenue at Broadway, is a few blocks from Loyola&rsquo;s main campus. Major renovations were completed to two existing buildings in 1986, creating modernized housing for the School of Law and Law Library.</p>
          <p>In 1986, a 115,000-square-foot Communications/Music Building was dedicated. The building, constructed on the corner of St. Charles Avenue and Calhoun Street, houses the Department of Communications and the College of Music. The building boasts, in addition to the latest technology for broadcasting and music studios, the 600-seat Louis J. Roussel Performance Hall.</p>
          <p>The six-level Recreational Sports Complex was dedicated in February 1988. The RecPlex includes two floors of racquetball, tennis, basketball, and volleyball courts; a natatorium with diving pool, whirlpool, sauna, and steam room; an elevated jogging track and weight room. The building also houses a four-story parking garage.</p>
          <p>The Activities Quad, between Bobet Hall and the Danna Center, was renamed the Plaza De Los Martires De La Paz in 1989 to honor the six Jesuits, their cook, and her daughter who were slain in El Salvador. The Jesuits taught at the University of San Salvador. Eight trees were planted in the Peace Quad as a permanent memorial to these contemporary martyrs.</p>
          <p>In 1989, historic Greenville Hall on the Broadway campus was renovated to provide office space for the Division of Institutional Advancement (alumni/parent relations, development, and public affairs/publications/marketing communications). This outstanding Italianate structure was built in 1892 for St. Mary&rsquo;s Academy, a girls&rsquo; school established in 1861 by Dominican nuns from Cabra, Ireland. In 1864 when the nuns acquired the property on which the building sits, the area was known as the village of Greenville, a community which was annexed by the City of New Orleans in 1870. In 1910, the academy became St. Mary&rsquo;s Dominican College. In 1984, the same year Loyola bought the Broadway campus, Greenville was designated a historic landmark by the Orleans Parish Landmarks Commission.</p>
          <p>Loyola&rsquo;s Broadway campus today also includes the School of Law, Cabra Residence Hall, and the Department of Visual Arts in St. Mary&rsquo;s Hall.</p>
          <p>In 1993, Loyola purchased Mercy Academy at the corner of Calhoun and Freret streets. The facility was renovated in 1994 &ndash; 95 and a number of departments moved in including the Office of Human Resources, the Office of International Student Affairs, the Women&rsquo;s Resource Center, and Physical Plant.</p>
          <p>In 1996, Loyola officially changed its name to Loyola University New Orleans to distinguish itself from other Jesuit institutions with similar names.</p>
          <p>Loyola continues to grow and expand physically. A new 500-car parking garage was completed on West Road in 1996. The 150,000-square-foot, 550,000-volume-capacity J. Edgar and Louise S. Monroe Library opened its doors in January 1999 and was dedicated in February 1999. Thresholds: The Campaign for Loyola University New Orleans supported the library project and provided funding endowment for faculty and staff support and endowment for student financial aid. The $50 million capital campaign, the largest in Loyola&rsquo;s history, exceeded its goal within its established five-year framework (1993 &ndash; 1998) with a total of over $51 million raised. Carrollton residence hall was also completed in 1999.</p>
          <p>In 2003, athletic scholarships were once again awarded to men&rsquo;s and women&rsquo;s basketball players.</p>
          <p>In 2006, following the devastating effect of Hurricane Katrina on New Orleans, Loyola implemented Pathways: Toward Our Second Century. This plan restructured the existing colleges and departments into five distinct colleges: Business, Humanities and Natural Sciences, Law, Music and Fine Arts, and Social Sciences. Under the plan, the School of Mass Communication and the School of Nursing were also created and housed under the College of Social Sciences.</p>
          <p>In 2007, the College of Law opened its Wendell H. and Anne B. Gauthier Family Wing. The four-story, 16,000-square-foot addition, located at the corner of Pine and Dominican Streets, seamlessly connected to the main law building.</p>
          <p>Also in 2007, the Danna Center was renamed the Danna Student Center, and the Recreational Sports Complex became the University Sports Complex.</p>
          <p>In 2008, Loyola completed an extensive renovation of the Danna Student Center. In addition, two classrooms in Bobet Hall were redone, and the Gregory R. Choppin Chemistry Wing in Monroe Hall was renovated.</p>
          <p>In fall 2010, the university rededicated the former university library as the new Tom Benson Jesuit Center. Mr. Benson&rsquo;s investment of $8 million in the university will go toward the renovation of the 36,000 square foot building, originally constructed in 1950, which will house the Jesuit Center and a new chapel. </p>
          <p> In 2011, two additional floors were completed on the West Road Garage, creating 236 more parking spaces for the university&rsquo;s main campus.</p>
          <p> Also in 2011, the university completed a renovation and expansion of Thomas Hall, which formerly housed Jesuits on campus. The renovated building is now a one-stop-shop for student administrative services, including Admissions, Bursar&rsquo;s Office, Student Records, Student Finance and Financial Aid.</p>
          <p> The College of Law expanded its reach in 2011 with the renovation of the former Dominican Conference Center. The university purchased the building in 2009 and completed its renovation in the summer of 2011. It now houses the Stuart H. Smith Law Clinic and Center for Social Justice. </p>
          <p>Loyola University New Orleans is one of 28 Jesuit colleges and universities in the United States. It is open to students of all faiths.</p>
    </div>
  
  </div><!-- container -->

</div><!-- #body-wrap -->


<?php 
    include( '../../../includes/footer.php'); 
    include( '../../../includes/more-menu.php'); 
    include( '../../../includes/javascript.php'); 
?>

</body>
</html>