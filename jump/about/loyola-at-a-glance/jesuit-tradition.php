<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Jesuit Tradition - Loyola University New Orleans</title>
    <link type="text/css" rel="stylesheet" href="http://fast.fonts.net/cssapi/d5e07912-c037-4030-b268-eb9fd671dc66.css" />
    <!-- Bootstrap -->
    <link href="/css/bootstrap.css" rel="stylesheet">
    <!--<link rel="stylesheet" type="text/css" href="styles.css"> -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="./ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- rich preview meta -->
    <meta name="description" content="Home page of Loyola University New Orleans" />
    <meta property="og:title" content="Loyola University New Orleans Home Pagew" />
    <meta property="og:description" content="Loyola University New Orleans is the best." />
    <meta property="og:image" content="./img/loyno_havoc.jpg" />

    <link rel="stylesheet" type="text/css" href="/fonts/faith-and-glory-one/faith-and-glory-one.css">
    <link rel="stylesheet" type="text/css" href="/fonts/faith-and-glory-two/faith-and-glory-two.css">
    <link rel="stylesheet" href="/css/flickity.css" media="screen">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <?php require('../../../includes/header.php'); ?>

<div id="body-wrap-lp">
  <div id="landingtop">
      <div class="container">
        <div class="landbox">
            <ul>
                <li><a href="demographics.php">Demographics</a></li>
                <li><a href="rankings-and-honors.php">Rankings + Honors</a></li>
                <li><a href="loyola-history.php">Loyola History</a></li>
                <li><a href="jesuit-tradition.php">Jesuit Tradition</a></li>
            </ul>
        </div>
      </div>
  </div>
  <div class="landingSection">
    <div class="container">
      <div class="col-md-9"> 
        	<h2>Jesuit Tradition</h2>
            <p>Loyola's rich history dates back to 1540, when Saint Ignatius of Loyola founded the Society of Jesus, whose members are called Jesuits. From the beginning, Jesuits have held that scholarly excellence plays an integral role in helping men and women achieve moral excellence. For more than 450 years, excellence in education has been an essential focus of the Jesuits. It was with this focus that the Jesuits first arrived among the earliest settlers in New Orleans and Louisiana, eventually establishing what would become Loyola University and continuing the Jesuit tradition of creating centers of education.</p>
            <p>The Jesuit educational network is one of the largest systems in American higher education, with more than 200,000 students currently enrolled in the 28 U.S. Jesuit universities. Worldwide, Jesuit universities and colleges have graduated more than 1,000,000 students.</p>
            <h3>What is the Jesuit vision of education?</h3>
            <p>Jesuit education is a call to human excellence, to the fullest possible development of all human qualities. This implies a rigor and academic excellence that challenges the student to develop all of his or her talents to the fullest. It is a call to critical thinking and disciplined studies, a call to develop the whole person, head and heart, intellect and feelings.</p>
            <p>The Jesuit vision of education implies further that students learn how to be critical, examine attitudes, challenge assumptions, and analyze motives. All of this is important if they are to be able to make decisions in freedom, the freedom that allows one to make love-filled and faith-filled decisions.</p>
            <h3>The Jesuit Ideals</h3>
            <p>In front of Loyola University New Orleans' <a href="http://library.loyno.edu">J. Edgar and Louise S. Monroe Library</a>, there is a walkway, a joint gift of the classes of 2002 and 2003, which reminds all who walk campus of the Jesuit ideals of:</p>

            <ul>
              <li>Pursuit Of Excellence</li>
              <li>Respect For The World, Its History And Mystery</li>
              <li>Learning From Experience</li>
              <li>Contemplative Vision Formed By Hope</li>
              <li>Development Of Personal Potential</li>
              <li>Critical Thinking And Effective Communication</li>
              <li>Appreciation Of Things Both Great And Small</li>
              <li>Commitment To Service</li>
              <li>Special Concern For The Poor And Oppressed</li>
              <li>Linking Faith With Justice</li>
              <li>International And Global Perspective</li>
              <li>Discerning Mindset: Finding God In All Things</li>
            </ul>

      </div>
 
    <div class="col-md-3">
      <img class="iso" src="images/loyola-university-new-orleans-ignatius-loyola-statue.jpg" alt="Loyola University New Orleans" />
    </div>
  </div><!-- container -->

</div><!-- #body-wrap -->



<?php 
    include( '../../../includes/footer.php'); 
    include( '../../../includes/more-menu.php'); 
    include( '../../../includes/javascript.php'); 
?>

</body>
</html>