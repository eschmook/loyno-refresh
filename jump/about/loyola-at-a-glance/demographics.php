<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-P4ZQPP');</script>
    <!-- End Google Tag Manager -->
    <title>Demographics - Loyola University New Orleans</title>
    <link type="text/css" rel="stylesheet" href="http://fast.fonts.net/cssapi/d5e07912-c037-4030-b268-eb9fd671dc66.css" />
    <!-- Bootstrap -->
    <link href="/css/bootstrap.css" rel="stylesheet">
    <!--<link rel="stylesheet" type="text/css" href="styles.css"> -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="./ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- rich preview meta -->
    <meta name="description" content="Demographics of Loyola University New Orleans" />
    <meta property="og:title" content="Loyola University New Orleans Demographics" />
    <meta property="og:description" content="A leading Catholic, Jesuit university, Loyola offers students from all faith traditions a campus environment rich with both spirituality and academic inquiry." />
    <meta property="og:image" content="./img/loyno_havoc.jpg" />

    <link rel="stylesheet" type="text/css" href="/fonts/faith-and-glory-one/faith-and-glory-one.css">
    <link rel="stylesheet" type="text/css" href="/fonts/faith-and-glory-two/faith-and-glory-two.css">
    <link rel="stylesheet" href="/css/flickity.css" media="screen">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <?php require('../../../includes/header.php'); ?>

<div id="body-wrap-lp">
  <div id="landingtop">
      <div class="container">
        <div class="landbox">
            <ul>
                <li><a href="demographics.php">Demographics</a></li>
                <li><a href="rankings-and-honors.php">Rankings + Honors</a></li>
                <li><a href="loyola-history.php">Loyola History</a></li>
                <li><a href="jesuit-tradition.php">Jesuit Tradition</a></li>
            </ul>
        </div>
      </div>
  </div>
  <div class="landingSection">
    <div class="container"> 

          <h2>Demographics</h2>
          <h4>Freshmen</h4>
          <ul>
            <li>38% male/62% female</li>
            <li>Ethnic minorities represent 37% of all freshmen</li>
            <li>63 percent are from out of state; 44 states (plus DC, PR, VI), 2 percent are international; 7 countries are represented</li>
            <li>99.5 percent are full-time; .5 percent part-time</li>
            <li>The average high school grade point average of our freshmen is 3.58</li>
            <li>SAT scores (middle 50%) Critical Reading 520&ndash;630, Math 500 &ndash; 610 </li>
            <li>ACT scores (middle 50%) range from 23-29 (71% of freshmen submitted the ACT</li>
            <li>615 freshmen students, 25% of the total undergraduates population are new freshmen</li>
            <li>84% freshmen live on campus; 16% commute</li>
            <li>80 percent of freshmen receive some form of financial aid</li>
          </ul>
          
          <h4>Undergraduate</h4>
          <ul>
            <li>40% male/60% female</li>
            <li>Ethnic minorities represent 39% of all freshmen</li>
            <li>57 percent are from out of state; 48 states (plus DC, PR, VI), 4 percent are international; 39 countries are represented</li>
            <li>94 percent are full-time; 6 percent part-time</li>
            <li>2,506 undergraduate students, 65% of the total population is undergraduate</li>
            <li>49 percent of undergraduates live on campus; 51% commute</li>
            <li>89 percent of undergraduates receive some form of financial aid </li>
          </ul>

          <h4>Graduate</h4>
          <ul>
            <li>21 percent male; 79 percent female</li>
            <li>Ethnic minorities represent 27 percent of student body</li>
            <li>60 percent are from out of state; 44 states (including DC, PR, VI) are represented</li>
            <li>1 percent are international; 12 countries are represented</li>
            <li>59 percent are full-time; 41 percent part-time</li>
            <li>635 graduate students, 134 continuing education students are enrolled; graduate population 17% of the total enrollment</li>
            <li>&lt; 1 percent of graduate students live on campus</li>
          </ul>
          <h4>Doctoral</h4>
          <ul>
            <li>6 percent male; 94 percent female</li>
            <li>Ethnic minorities represent 38 percent of student body</li>
            <li>66 percent are from out of state; 20 states (including DC, PR, VI) are represented</li>
            <li>0 percent are international; 1 countries are represented</li>
            <li>87 percent are full-time; 13 percent part-time</li>
            <li>82 Doctorate students; Doctoral students make up 2% of the total enrollment</li>
            <li>0 percent of doctoral students live on campus</li>
          </ul>
          <h4>Law</h4>
          <ul>
            <li>47 percent male; 53 percent female</li>
            <li>Ethnic minorities represent 29 percent of student body</li>
            <li>23 percent are from out of state; 28 states (including DC, PR, VI) are represented</li>
            <li>2 percent are international; 8 countries are represented</li>
            <li>79 percent are full-time; 21 percent part-time</li>
            <li>479 law students are enrolled; law students make up 12% of the total enrollment</li>
            <li>4 percent of Law students live on campus</li>
          </ul>
          <p><strong>Updated December 2016 with Fall 2016 data; next upate Fall 2017 </strong></p>
          <p>Source: Office of Institutional Research and Effectiveness Fact Book</p>
      </div>

  </div>

</div> <!-- #body-wrap -->


<?php 
    include( '../../../includes/footer.php'); 
    include( '../../../includes/more-menu.php'); 
    include( '../../../includes/javascript.php'); 
?>

</body>
</html>