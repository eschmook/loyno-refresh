<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  	<title>Loyola at a Glance - Loyola University New Orleans</title>
    <link type="text/css" rel="stylesheet" href="http://fast.fonts.net/cssapi/d5e07912-c037-4030-b268-eb9fd671dc66.css" />
    <!-- Bootstrap -->
    <link href="/css/bootstrap.css" rel="stylesheet">
    <!--<link rel="stylesheet" type="text/css" href="styles.css"> -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="./ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- rich preview meta -->
    <meta name="description" content="Loyola at a Glance Loyola University New Orleans" />
    <meta property="og:title" content="Loyola University New Orleans Loyola at a Glance" />
    <meta property="og:description" content="A leading Catholic, Jesuit university, Loyola offers students from all faith traditions a campus environment rich with both spirituality and academic inquiry." />
    <meta property="og:image" content="./img/loyno_havoc.jpg" />

    <link rel="stylesheet" type="text/css" href="/fonts/faith-and-glory-one/faith-and-glory-one.css">
    <link rel="stylesheet" type="text/css" href="/fonts/faith-and-glory-two/faith-and-glory-two.css">
    <link rel="stylesheet" href="/css/flickity.css" media="screen">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body> 

    <?php require('../../../includes/header.php'); ?>

<div id="body-wrap-lp">
	<div id="landingtop">
	    <div class="container">
		    <div class="landbox">
		        <ul>
	              <li><a href="demographics.php">Demographics</a></li>
	              <li><a href="rankings-and-honors.php">Rankings + Honors</a></li>
	              <li><a href="loyola-history.php">Loyola History</a></li>
	              <li><a href="jesuit-tradition.php">Jesuit Tradition</a></li>
		        </ul>
		    </div>
	    </div>
	</div>
	<div class="landingSection">
	    <div class="container">
	    	<h2>Loyola at a Glance</h2>
	   	    
	   	        <ul>
	   	          <li><strong>Chartered:</strong> 1912</li>
	   	          <br />
	              <li><strong>Type: </strong>Private, Catholic, Jesuit comprehensive university; open to students of all faiths</li>
	              <br />
	              <li><strong>Enrollment:</strong> 3,836 (total): 2,506 undergraduates; 635 graduate students; 82 Doctoral students; 479 law students; and 134 continuing education students</li>
	              <br />
	              <li><strong>Faculty:</strong> 91 percent of full-time faculty hold highest degree.</li>
	              <br />
	              <li><strong>Student-Faculty Ratio: </strong>10:1</li>
	              <br />
	              <li><strong>Five Colleges:</strong> Arts and Sciences, Business, Graduate and Professional Studies, Law, and Music and Fine Arts</li>
	              <br />
	              <li><strong>Degree Programs: </strong>59 undergraduate programs and 11 graduate and professional programs</li>
	              <br />
	              <li><strong>50+ Study Abroad Countries:</strong> Belgium, Brazil, China, Ecuador, France, India, Ireland, Italy, Japan, Korea, Italy, Netherlands, Spain, U.K., and more. </li>
	              <br />
	              <li><strong>Campus:</strong> 24 acres on historic oak-lined St. Charles Avenue in Uptown New Orleans; directly across from Audubon Park and Audubon Zoo, with expansive green space and lagoons, golf course, tennis courts and riding stables; 20 minutes from the French Quarter and downtown New Orleans.</li>
	              <br />
	              <li><strong>Financial Aid and Scholarships: </strong>Approximately 89 percent of students receive some form of financial aid.</li>
	              <br />
	              <li><strong>Cost: </strong>Annual  tuition (2017-2018): $37,676; room and board: $13,214; resident fees: $1,566</li>
	              <br />
	              <li><strong>Admissions:</strong> Average GPA: 3.58; Test scores (middle 50%): ACTC  23-29; SAT Critical Reading 520-630 / Math 500-610</li>
	              <br />
	              <li><strong>Athletics</strong>: Compete in Division I of the National Association of Intercollegiate Athletics with 16 varsity sports.</li>
	              <br />
	              <li><strong>Student Life</strong>: 130 student organizations, clubs and intramural sports</li>
	              <br />
	              <li><strong>Accreditations:</strong> Southern Association of Colleges and Schools (SACS); Louisiana State Board of Nursing (LSBN); The Council for Accreditation of Counseling and Related Educational Programs (CACREP); Association to  Advance Collegiate Schools of Business (AACSB); National Association of Schools of Music (NASM); American Bar Association (ABA); Association of American Law Schools (AALS); American Chemical Society (ACS); Commission of Collegiate Nursing Education (CCNE); Accrediting Council on Education in Journalism and Mass Communications (ACEJMC); and Certification in Education for Public Relations (CEPR), Accreditation for Education in Nursing (ACEN).</li>
	            </ul>
	            <p><strong>All facts are accurate as of Fall 2016 and valid through Fall 2017.</strong></p>
	            <p>Updated December 15, 2016<br />
	            </p>

	    </div>
	</div>

</div><!-- #body-wrap -->

<?php 
    include( '../../../includes/footer.php'); 
    include( '../../../includes/more-menu.php'); 
    include( '../../../includes/javascript.php'); 
?>

</body>
</html>