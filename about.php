<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>About | Loyola University New Orleans</title>
    <link type="text/css" rel="stylesheet" href="http://fast.fonts.net/cssapi/d5e07912-c037-4030-b268-eb9fd671dc66.css" />
    <!-- Bootstrap -->
    <link href="./css/bootstrap.css" rel="stylesheet">
    <!--<link rel="stylesheet" type="text/css" href="styles.css"> -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="./apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="./apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="./apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="./apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="./apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="./apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="./apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="./apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="./apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="./android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="./favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="./favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="./favicon-16x16.png">
    <link rel="manifest" href="./manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="./ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- rich preview meta -->
    <meta name="description" content="About Loyola University New Orleans" />
    <meta property="og:title" content="Loyola University New Orleans About Page" />
    <meta property="og:description" content="A leading Catholic, Jesuit university, Loyola offers students from all faith traditions a campus environment rich with both spirituality and academic inquiry." />
    <meta property="og:image" content="./img/loyno_havoc.jpg" />

    <link rel="stylesheet" type="text/css" href="./fonts/faith-and-glory-one/faith-and-glory-one.css">
    <link rel="stylesheet" type="text/css" href="./fonts/faith-and-glory-two/faith-and-glory-two.css">
    <link rel="stylesheet" href="./css/flickity.css" media="screen">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <?php require('includes/header.php'); ?>

    <div id="landing-about" class="landingSection hero">
    <div id="whiteOverlay"></div>
        <div class="container">
            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12">
            <h2>About </h2>
                <p>
                At Loyno, you define who you want to be: a free-thinker, an entrepreneur, a risk-taker, a scientist, a creative, and above all yourself — <strong>the thing no one else can be</strong>. This education lets you find that. It allows you to do more than transform yourself; it allows you to transform into yourself.</p>

                <a href="#linksStart" class="slowscrolling"><img src="./img/svg/arrow-down.svg" alt="arrow to read more" class="arrow_more pulse_animation" /></a>

            </div>

            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 stats">
              <div class="stat">
                <span class="fa svg-bg lightbulb"></span>
                  <div class="numberFact">#1</div>
                  <div>Best City in the United States For Creative Professionals <em>Smart Asset</em></div>
              </div>
 
              <div class="stat">
                  <span class="fa svg-bg regional"></span>
                    <div class="numberFact">#10</div>
                    <div>Best Regional Universities of the South <em>U.S. News and World Report</em></div>
              </div>    
            </div>
            
        </div>
    </div>

    <div class="landingSection" id="linksStart">
        <div class="container items">
                <div class="landcol col-md-6 col-xs-12">
                    <div class="landbox col-md-12">
                        <h4>Loyola at a Glance</h4>

                        <ul>
                            <li><a href="jump/about/loyola-at-a-glance/index.php">Facts</a>
                            </li>
                            <li><a href="jump/about/loyola-at-a-glance/demographics.php">Demographics</a>
                            </li>
                            <li><a href="jump/about/loyola-at-a-glance/rankings-and-honors.php">Rankings + Honors</a>
                            </li>
                            <li><a href="jump/about/loyola-at-a-glance/loyola-history.php">History</a>
                            </li>
                            <li><a href="jump/about/loyola-at-a-glance/jesuit-tradition.php">Jesuit Tradition</a>
                            </li>

                        </ul>

                    </div>

                    <div class="landbox col-md-12">
                        <h4>Jesuit Identity</h4>
                        <h5><em>Programs + Ministries</em></h5>

                        <ul>
                            <li><a href="http://mm.loyno.edu/university-ministry">University Ministry</a>
                            </li>
                            <li><a href="http://mm.loyno.edu/volunteer-service/loyola-university-community-action-program">Loyola University Community Action Program</a>
                            </li>
                            <li><a href="http://mm.loyno.edu/volunteer-service/ignacio-volunteer-programs">Ignacio Volunteer Programs</a>
                            </li>
                            <li><a href=" http://mm.loyno.edu/university-ministry/law-ministries">Law Ministries</a>
                            </li>
                            <li><a href="http://mm.loyno.edu/university-ministry/interfaith-ministries">Interfaith Ministries</a>
                            </li>

                        </ul>

                        <h5><em>Services</em></h5>

                        <ul>
                            <li><a href="http://mm.loyno.edu/university-ministry/retreats">Retreats</a>
                            </li>
                            <li><a href="http://mm.loyno.edu/university-ministry/christian-life-communities">Christian Life Communities</a>
                            </li>
                            <li><a href="http://mm.loyno.edu/volunteer-service">Volunteer Service</a>
                            </li>
                            <li><a href="http://mm.loyno.edu/university-ministry/sacraments-faith-development">Sacraments + Faith Development</a>
                            </li>
                            <li><a href="http://mm.loyno.edu/university-ministry/area-church-services">Area Church Services</a>
                            </li>
                            <li><a href="http://mm.loyno.edu/staff/resident-chaplains">Find a Chaplain</a>
                            </li>
                        </ul>

                        <h5><em>Centers + Institutes</em></h5>

                        <ul>
                            <li><a href="http://president.loyno.edu/chaplain">University Chaplain</a>
                            </li>
                            <li><a href="http://lim.loyno.edu/">Loyola Institute for Ministry</a>
                            </li>
                            <li><a href="http://www.loyno.edu/jsri/">Jesuit Social Research Institute</a>
                            </li>
                            <li><a href="http://www.loyno.edu/twomey/">Twomey Center</a>
                            </li>
                            <li><a href="http://www.loyno.edu/spiritual-capital/">Center for Spiritual Capital</a>
                            </li>
                            <li><a href="http://president.loyno.edu/chaplain/jesuit-community">Jesuit Community</a>
                            </li>
                        </ul>

                    </div>

                </div>
                <!--endleftcol-->

                <div class="landcol col-md-6 col-xs-12">
                    <div class="landbox col-md-12">
                        <h4>Visitors</h4>

                        <ul>
                            <li><a href="jump/about/visitors/map-loyola.php">Campus Maps</a>
                            </li>
                            <li><a href="http://www.loyno.edu/search/">University Contacts </a>
                            </li>
                            <li><a href="jump/about/visitors/map-new-orleans.php">New Orleans Map + Driving Directions</a>
                            </li>
                            <li><a href="/neworleans.php">More about New Orleans</a>
                            </li>
                            <li class="ghost"><a></a></li>
                            <li class="ghost"><a></a></li>


                        </ul>
                    </div>

                    <div class="landbox col-md-12">
                        <h4>Media Information</h4>

                        <ul>
                            <li><a href="http://www.loyno.edu/news/">News Releases</a>
                            </li>
                            <li><a href="http://www.loyno.edu/experts">Guide to the Experts</a>
                            </li>
                            <li><a href="http://www.loyno.edu/gallery/gallery-popup.php">Photo Galleries</a>
                            </li>
                            <li><a href="http://www.loyolamaroon.com/">Student News Media</a>
                            </li>

                        </ul>
                    </div>
                    <div class="landbox col-md-12">
                        <h4>Administration</h4>

                        <ul>
                            <li><a href="http://www.loyno.edu/boardoftrustees/">Board of Trustees</a>
                            </li>
                            <li><a href="http://president.loyno.edu/">President</a>
                            </li>
                            <li><a href="http://academicaffairs.loyno.edu/">Academic Affairs</a>
                            </li>
                            <li><a href="http://enrollment.loyno.edu/">Enrollment Management</a>
                            </li>
                            <li><a href="http://finance.loyno.edu/">Finance + Administration</a>
                            </li>
                            <li><a href="http://advancement.loyno.edu/">Institutional Advancement</a>
                            </li>
                            <li><a href="http://mm.loyno.edu/">Mission + Ministry</a>
                            </li>
                            <li><a href="http://studentaffairs.loyno.edu/">Student Affairs</a>
                            </li>

                        </ul>
                    </div>
                </div>
                <!--endrightcol-->

            </div>
        </div>
    </div>

    <div id="landingbottom">
        <div class="container">

        <div class="landbox col-md-6 col-xs-12">

            <ul>
                <li><a href="http://www.loyno.edu/mission-statements/university-identity-statement.php">University Identity Statement</a>
                </li>
                <li><a href="http://strategicplan.loyno.edu">Strategic Plan</a>
                </li>
                <li><a href="http://www.loyno.edu/mission-statements">Mission Statements</a>
                </li>            
            </ul>
        </div>
        </div>
    </div>
    <!--end loynoFacts-->

<?php 
    include( 'includes/footer.php'); 
    include( 'includes/more-menu.php'); 
    include( 'includes/javascript.php'); 
?>

</body>
</html>