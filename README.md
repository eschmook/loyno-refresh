# [Loyno.edu](http://www.loyno.edu/) - Refresh 2017 - Coding Class example

This is a copy of the Refresh of the Loyola University New Orleans Homepage and main landing pages for the summer Coding Class.

### How do I get set up? ###

This site requires a local webserver running PHP.  The CSS is compiled using [SASS](http://sass-lang.com/) and [Compass](http://compass-style.org/).

* Install Compass
* PHP
* Local Webserver

### Who do I talk to? ###

* ejschmid@loyno.edu
* mndoughe@zmail.loyno.edu