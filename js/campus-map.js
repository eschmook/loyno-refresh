// 

$(document).ready(function() {
    
	var mark;
	var pointA;
	var pointB;
	
	if (GBrowserIsCompatible()) {
		var m = $("#map")[0];
        if(m) {
			
			var map = new GMap2(m);
			var start = new GLatLng(29.934714,-90.12144);
			var zoomLevel = 18;
			map.setCenter(start, zoomLevel);
			map.addControl(new GSmallMapControl());
			
			/*
			GEvent.addListener(map, 'click', function(overlay, point){
				if(mark) {
					map.removeOverlay(mark);
				}
				if(point) {
					pointA = new GPoint(point.x, point.y);
					mark = new GMarker(pointA);
					map.addOverlay(mark);
					map.getCenter(point);
					//$('#lng').attr('value',point.x);
					//$('#lat').attr('value',point.y);
					$('#lat').html(point.y);
					$('#lng').html(point.x);
				}
			});
			*/
			
			$('#select-building').change(function(){
				// check if there is a value
				if($(this).val().length == 0) { return false; }
				
				// when there is time swap with a better non-hard-coded solution
				if($(this).val() == "Broadway Campus" || 
					$(this).val() == "Broadway Activities Center" ||
					$(this).val() == "College of Law" ||
					$(this).val() == "Greenville Hall" ||
					$(this).val() == "Cabra Hall" ||
					$(this).val() == "St. Marys Hall/Visual Arts") { 
					toggleToBroadwayCampus(); 
				} else {
					toggleToMainCampus();
				}
				
				moveToMapLocation('building='+$(this).val());
			});
			
			// Tab Links
			$('#campus-map .main-campus').click(function(){
				moveToMapLocation('building=Main Campus'); // assume location name is static
				toggleToMainCampus();
			});
			$('#campus-map .broadway').click(function(){
				moveToMapLocation('building=Broadway Campus'); // assume location name is static
				toggleToBroadwayCampus();
			});
			function toggleToMainCampus() {
				$('#campus-map .broadway').removeClass('active');
				$('#campus-map .main-campus').addClass('active');
			}
			function toggleToBroadwayCampus() {
				$('#campus-map .main-campus').removeClass("active");
				$('#campus-map .broadway').addClass("active");
			}
			
			function moveToMapLocation(b) {
				$.ajax({
					type:	'POST',
					data:	b,
					url:	'/includes/campus_locations.php',
					success:function(r) {
						var cord = r.split("|");
						if(mark) {
							map.removeOverlay(mark);
						}
						pointB = new GLatLng(cord[0],cord[1]);
						mark = new GMarker(pointB);
						map.addOverlay(mark);
						map.setCenter(pointB, 18);
						//$('#lat').attr('value',cord[0]);
						//$('#lng').attr('value',cord[1]);
						$('#lat').html(cord[0]);
						$('#lng').html(cord[1]);
					}
				});
			}
			
		}
	}
});