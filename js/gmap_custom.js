// The following example creates complex markers to indicate beaches near
// Sydney, NSW, Australia. Note that the anchor is set to (0,32) to correspond
// to the base of the flagpole.
function initMap() {
   var customMapType = new google.maps.StyledMapType([{
       stylers: [
           { hue: '#cefbe1' },
           { visibility: 'simplified' },
           { gamma: 0.5 },
           { weight: 0.5 }
       ]
   }, {
       elementType: 'labels',
       stylers: [{ visibility: 'on' }]
   }, {
       featureType: 'water',
       stylers: [{ color: '#6caafe' }]
   }], {
       name: 'Custom Style'
   });
   var customMapTypeId = 'custom_style';

   var map = new google.maps.Map(document.getElementById('map'), {
       zoom: 15,
       scrollwheel: false,
       zoomControl: true,
       center: { lat: 29.93414, lng: -90.121721 },
       mapTypeControlOptions: {
           mapTypeIds: [google.maps.MapTypeId.ROADMAP, customMapTypeId]
       }
   });
   setMarkers(map);
   map.mapTypes.set(customMapTypeId, customMapType);
   map.setMapTypeId(customMapTypeId);
}
// Data for the markers consisting of a name, a LatLng and a zIndex for the
// order in which these markers should display on top of each other.
var locations = [
   ['Loyola University', 29.93414, -90.121721],
   ['Audubon Park', 29.9248688, -90.1317898],
   ['Jackson Square', 29.9574616, -90.0651438],
   ['Mercedes-Benz Superdome', 29.951061, -90.0834329],
   ['Commander’s Palace', 29.9288594, -90.0864324],
   ['Holy Name of Jesus', 29.9352541, -90.1230121]
];

function setMarkers(map) {
   // Adds markers to the map.
   // Marker sizes are expressed as a Size of X,Y where the origin of the image
   // (0,0) is located in the top left of the image.
   // Orig`, anchor positions and coordinates of the marker increase in the X
   // direction to the right and in the Y direction down.

   // Shapes define the clickable region of the icon. The type defines an HTML
   // <area> element 'poly' which traces out a polygon as a series of X,Y points.
   // The final coordinate closes the poly by connecting to the first coordinate.
   var shape = {
       coords: [1, 1, 1, 20, 18, 20, 18, 1],
       type: 'poly'
   };
   var infowindow = new google.maps.InfoWindow();
   for (var i = 0; i < locations.length; i++) {
       var location = locations[i];
       var marker = new google.maps.Marker({
           position: { lat: location[1], lng: location[2] },
           map: map,
           shape: shape
       });
       google.maps.event.addListener(marker, 'click', (function(mm, tt) {
           return function() {
               infowindow.setContent(tt);
               infowindow.open(map, mm);
           }
       })(marker, location[0]));

   }
}
//end all map stuff////////////////////////////////////////////////////////////
