<?php
 @require_once("includes/functions.php"); 

  if (checkProd()) {
    // do something if we are on www.loyno.edu
    // you can set this to any domain by modifying 
    // checkProd function in includes/functions.php
  }
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Loyola University New Orleans</title>
    <link type="text/css" rel="stylesheet" href="http://fast.fonts.net/cssapi/d5e07912-c037-4030-b268-eb9fd671dc66.css" />
    <!-- Bootstrap -->
    <link href="./css/bootstrap.css" rel="stylesheet">
    <!--<link rel="stylesheet" type="text/css" href="styles.css"> -->
    <!--<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="./apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="./apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="./apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="./apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="./apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="./apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="./apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="./apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="./apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="./android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="./favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="./favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="./favicon-16x16.png">
    <link rel="manifest" href="./manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="./ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- rich preview meta -->
    <meta name="description" content="Home page of Loyola University New Orleans" />
    <meta property="og:title" content="Loyola University New Orleans Home Page" />
    <meta property="og:description" content="A leading Catholic, Jesuit university, Loyola offers students from all faith traditions a campus environment rich with both spirituality and academic inquiry." />
    <meta property="og:image" content="./img/loyno_havoc.jpg" />

    <link rel="stylesheet" type="text/css" href="./fonts/faith-and-glory-one/faith-and-glory-one.css">
    <link rel="stylesheet" type="text/css" href="./fonts/faith-and-glory-two/faith-and-glory-two.css">
    <link rel="stylesheet" href="./css/flickity.css" media="screen">
    <script type="text/javascript" src="js/instafeed.min.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>

<body>
    <?php require_once( 'includes/header.php'); ?>
    <div id="sliderCarouselSection">
      <div class="slider-carousel">

      <?php renderSlider(); ?>

      </div><!-- slider-carousel -->


    </div><!-- main slider -->


        <div id="loynoFacts">

            <div class="container">
                <div class="col-md-12">
                  <div class="facts-carousel theFacts">
                      <div class="carousel-cell maroon">
                          <span class="fa svg-bg briefcase"></span>
                          <div class="numberFact">85%</div>
                          <div>of Loyola graduates have started their career or are enrolled in advanced study within six months of graduation.</div>
                      </div>
                      <div class="carousel-cell pink">
                          <span class="fa svg-bg globe"></span>
                          <div class="numberFact">1 in 3</div>
                          <div>Loyola students study abroad choosing from more than 50 countries</div>
                      </div>
                      <div class="carousel-cell black">
                          <span class="fa svg-bg users"></span>
                          <div class="numberFact">90%</div>
                          <div>of Loyola students participate in at least one experiential learning opportunity (internships, service learning)</div>
                      </div>
                      <div class="carousel-cell maroon">
                          <span class="fa svg-bg trophy"></span>
                          <div class="numberFact">Top 20</div>
                          <div>Fulbright Producer in 2015-2016 with a 20% application acceptance rate in 2017.</div>
                      </div>
                      <div class="carousel-cell pink">
                          <span class="fa svg-bg race-class"></span>
                          <div class="numberFact">#13</div>
                          <div>Lots of Race/Class Interaction <em>Princeton Review</em></div>
                      </div>
                      <div class="carousel-cell black">
                          <span class="fa svg-bg peace-corps"></span>
                          <div class="numberFact">Top Producer</div>
                          <div>Loyola was a Top Producer of Peace Corps Volunteers in 2017</div>
                      </div>
                      <div class="carousel-cell maroon">
                          <span class="fa svg-bg university"></span>
                          <div class="numberFact">Honor Roll</div>
                          <div>Loyola continues to be selected for the U.S. President’s Honor Roll for Community Engagement</div>
                      </div>
                      <div class="carousel-cell pink">
                          <span class="fa svg-bg money"></span>
                          <div class="numberFact">#12</div>
                          <div>Best Value in the South <em>U.S. News and World Report</em></div>
                      </div>
                      <div class="carousel-cell black">
                          <span class="fa svg-bg graduation-cap"></span>
                          <div class="numberFact">#10</div>
                          <div>Best Regional Universities of the South <em>U.S. News and World Report</em></div>
                      </div>
                      <div class="carousel-cell maroon">
                          <span class="fa svg-bg catholic-college"></span>
                          <div class="numberFact">#15</div>
                          <div>Top 50 Best Catholic Colleges and Universities <em>College Choice</em></div>
                      </div>
                      <div class="carousel-cell pink">
                          <span class="fa svg-bg brainpower"></span>
                          <div class="numberFact">#1</div>
                          <div>America's Best New Brainpower Cities, <em>Forbes</em></div>
                      </div>
                      <div class="carousel-cell black">
                          <span class="fa svg-bg plane"></span>
                          <div class="numberFact">#5</div>
                          <div>City Where Millennials Are Moving <em>Time</em></div>
                      </div>
                  </div>
                </div>
            </div>

        </div>
        <!--end loynoFacts-->

        <div id="academics" class="full-width-bg">
            <div class="container">
            <div class="col-lg-12 col-md-12">
                <h3 class="big-school-feel white">
                  <div class="headingLine">big school feel, small school</div>
                  <div class="headingLine text-style-1">advantage</div>
                </h3>

                <div class="main">
                    <p>State-of-the-art resources meet focused experiential learning</p>

                    <div class='action'>
                        <ul>
                            <li><a href="http://apply.loyno.edu/majors-degrees-programs">Majors, Degrees, &amp; Programs</a>
                            </li>
                            <li><a href="http://apply.loyno.edu/academic-opportunities">Academic Opportunities</a>
                            </li>
                            <li><a href="http://apply.loyno.edu/career-preparation">Career Preparation</a>
                            </li>
                            <li><a href="http://apply.loyno.edu/global-experiences">Global Experience</a>
                            </li>

                        </ul>

                        <p><a class="more" href="./academics.php">more academics</a>
                        </p>

                    </div>
                    <!--endaction-->
                </div>
                <!--endmain-->
            </div>
            </div>
        </div>
        <!--endacademics-->

        <div class="newMajors">
            <div class="container">
                <div class="col-md-12">
                    <a href="/new-majors/" class="majorsButton"><img src="./img/svg/Callout_NewMajors_Desktop2.svg" alt="New Majors! Learn More Here" class="img-responsive" /> </a>
                </div>
            </div>
        </div>

        <div id="colleges-programs" class="offset featuredSection">

            <div class="container">
            <div class="col-md-12 col-xs-12">
                <div class="highlights_and_such">
                    <h3>Program Highlights</h3>
                    <h4 class="all_programs"><a href="http://apply.loyno.edu/majors-degrees-programs">all programs </a></h4>
                </div>
             
             <div class="college-carousel thePack">
                        <div class="hovereffect carousel-cell business flipme featured-factoid">
                            <figure>
                                <figcaption>
                                    <h5>Business</h5>
                                    <p>Ranked in the top 100 cities on Forbes' list of “The Best Places for Business and Careers,” New Orleans is quickly becoming a hub for entrepreneurs. You'll participate in our Business Portfolio Program, which offers networking events, resume workshops, and mock interviews. Additionally, all of our students graduate with internship experience, so by the time you're looking for a job—you'll have already had one. </p>
                                    <a href="http://www.business.loyno.edu/programs-overview">Learn More</a>
                                </figcaption>
                            </figure>
                            <h4>Business</h4>
                        </div>

                        <div class="hovereffect carousel-cell music flipme featured-factoid">
                           <figure>
                                <figcaption>
                                    <h5>Music</h5>
                                    <p>Our music performance program will sharpen your skills in your preferred genre as well as challenge you to become competent in others. That way, your artistic voice is not just proficient—it's uniquely explored, informed, and realized. We believe in fostering your genius, pushing your aptitude to its highest potential—and also in giving you the tools you need to make your passion into a successful career.</p>
                                    <a href="http://cmfa.loyno.edu/music/programs-study ">Learn More</a>
                                </figcaption>
                            </figure>                    
                            <h4>Music</h4>
                        </div>
                        <div class="hovereffect carousel-cell food-studies flipme featured-factoid">
                            <figure>
                                <figcaption>
                                    <h5>Food Studies</h5>
                                    <p>Food is not just a way to stay alive; it's a way to feel alive. Through courses and experiential learning opportunities that offer an integrated education in food policy, commerce, and culture, the program produces graduates knowledgeable about the food system as a whole, preparing students for careers in fields as diverse as policymaking, food policy advocacy, food journalism, food criticism, food entrepreneurship, and consulting, among others.</p>
                                    <a href="http://cas.loyno.edu/food-studies">Learn More</a>
                                </figcaption>
                            </figure>                      
                            <h4>Food Studies</h4>
                        </div>

                        <div class="hovereffect carousel-cell digital-filmmaking flipme featured-factoid">
                            <figure>
                                <figcaption>
                                    <h5>Digital Filmmaking</h5>
                                    <p>Filmmaking is storytelling. And your passion drives you to share your ideas. Our Digital Filmmaking program is planted in the heart of Hollywood South with major films working day and night both on and around our beautiful campus. Join Loyola University New Orleans and you'll develop relationships, manage projects, create original works, and learn to market yourself as a professional. </p>
                                    <a href="http://cmfa.loyno.edu/music-industry-studies/digital-filmmaking">Learn More</a>
                                </figcaption>
                            </figure>                      
                            <h4>Digital Filmmaking</h4>
                        </div>

                        <div class="hovereffect carousel-cell comp-sci flipme featured-factoid">
                            <figure>
                                <figcaption>
                                    <h5>Computer Science</h5>
                                    <p>With a few lines of code you can do incredible things—build a billion-dollar company, create global networks of people, inspire or fund massive social movements. You will learn more than a practical skillset. You will learn a new language. At Loyno, you can become one of those game-changers.</p>
                                    <a href="http://cas.loyno.edu/mathematics/bachelor-science-computer-science">Learn More</a>
                                </figcaption>
                            </figure>     
                            <h4>Computer Science</h4>
                        </div>

                        <div class="hovereffect carousel-cell mass-comm flipme featured-factoid">
                            <figure>
                                <figcaption>
                                    <h5>Mass Communication</h5>
                                    <p>Telling a great story is more than finding the action — it's learning the language and finding your voice. Loyno can help you find and shape your voice. Our students have won first place nine times in the national Bateman PR case study competition—more than any other school in the country. Our student newspaper, The Maroon, is one of the most award-winning student papers in the country. We will encourage you to think. To analyze. To question. To create. </p>
                                    <a href="http://cas.loyno.edu/masscomm/programs-study">Learn More</a>
                                </figcaption>
                            </figure>                      
                            <h4>Mass Communication</h4>
                        </div>


                    </div><!-- end pack-carousel -->
            </div><!-- col-md-12 -->
            </div>
        </div>

        <div id="seeYourFutur" class="photo-based featuredSection">

            <div class="container">
                <div class="col-md-12 col-xs-12 ">
                    <h3>Award Winners</h3>
                    <p>We continue to be a top Fulbright producer in the U.S. See what some of our outstanding students are up to below:</p>
                </div>

                <div class="col-md-12 col-xs-12">
                    <div class="future-carousel theFuture">
                        <div class="hovereffect carousel-cell">
                            <img class="img-responsive" src="./img/homepage/Fulbright_lauren-stroh.jpg" alt="Lauren Stroh Photo" />

                            <div class="overlay photo-hover-content">
                                <p class="photo-quote name">
                                    Lauren Stroh<br />
                                    Fulbright Recipient
                                </p>
                                <a class="white-arrow" href="http://www.loyno.edu/news/story/2017/4/11/3916"></a>

                            </div>
                            
                        </div><!-- end carousel-cell -->

                        <div class="hovereffect carousel-cell">
                            <img class="img-responsive" src="./img/homepage/Fulbright_mathew_holloway.jpg" alt="Matthew Holloway Photo" />

                            <div class="overlay photo-hover-content">
                                <p class="photo-quote name">
                                    Matthew Holloway<br />
                                    Fulbright Recipient
                                </p>
                                <a class="white-arrow" href="http://www.loyno.edu/news/story/2017/4/10/3913"></a>

                            </div>
                        </div>


                        <div class="hovereffect carousel-cell">
                            <img class="img-responsive" src="./img/homepage/Fulbright_natalie-jones.jpg" alt="Natalie Jones Photo" />

                            <div class="overlay photo-hover-content">
                                <p class="photo-quote name">
                                    Natalie Jones<br />
                                    Fulbright Recipient
                                </p>
                                <a class="white-arrow" href="http://www.loyno.edu/news/story/2017/3/23/3914"></a>

                            </div>
               
                        </div><!-- end carousel-cell -->
                        <div class="hovereffect carousel-cell">
                            <img class="img-responsive" src="./img/homepage/Ignatian_Jennifer_Chamberlain.jpg" alt="Jennifer Chamberlain Photo" />

                            <div class="overlay photo-hover-content">
                                <p class="photo-quote name">
                                    Jennifer Chamberlain<br />
                                    Ignatian Scholarship
                                </p>
                                <a class="white-arrow" href="http://www.loyno.edu/news/story/2017/5/11/3936"></a>

                            </div>
               
                        </div><!-- end carousel-cell -->

                        <div class="hovereffect carousel-cell">
                            <img class="img-responsive" src="./img/homepage/Ignatian_Kate_Oleary.jpg" alt="Kate O'Leary Photo" />

                            <div class="overlay photo-hover-content">
                                <p class="photo-quote name">
                                    Kate O'Leary<br />
                                    Ignatian Scholarship
                                </p>
                                <a class="white-arrow" href="http://www.loyno.edu/news/story/2017/5/11/3938"></a>

                            </div>
               
                        </div><!-- end carousel-cell -->  

                        <div class="hovereffect carousel-cell">
                            <img class="img-responsive" src="./img/homepage/Ignatian_Michael_Paskevich.jpg" alt="Michael Paskevich Photo" />

                            <div class="overlay photo-hover-content">
                                <p class="photo-quote name">
                                    Michael Paskevich <br />
                                    Ignatian Scholarship
                                </p>
                                <a class="white-arrow" href="http://www.loyno.edu/news/story/2017/5/11/3937"></a>

                            </div>
               
                        </div><!-- end carousel-cell -->                                                                        
                  </div><!-- theFuture -->

                </div>
            </div><!--col md 10-->
  </div>   <!--see your future-->

        <!-- video -->
<div class="videoPlayer" id="myVideo">
    <div class="container">
    <div class=" col-lg-12 col-md-12">
      <div class="overlay">
        <h3 class="big-school-feel white">
          <div class="headingLine">a decision this <span class="text-style-1">big</span></div>
          <div class="headingLine" >can be this <span class="text-style-1">easy</span></div>
        </h3>
        <div class="main">
            <p>We're here to help you along the way. </p>

            <div class="action">
                <ul>
                    <li><a href="http://apply.loyno.edu/financial-aid">Financial Aid</a>
                    </li>
                    <li><a href="http://apply.loyno.edu/scholarships">Scholarships</a>
                    </li>
                    <li><a href="https://tcc.noellevitz.com/(S(yksvuqjll2ku3imdys5qmelp))/Loyola%20University%20New%20Orleans/Freshman-Students">Net Price Calculator</a>
                    </li>
                    <li><a href="http://apply.loyno.edu/bios">Talk To An Admissions Counselor</a>
                    </li>
                </ul>

                <p><a class="more" href="./admissions.php">more admissions</a>
                </p>

            </div>
            <!--endaction-->
        </div>

      </div> <!-- overlay -->
      </div>
    </div>

  <div class="bg-video"> 
      <div id="player"></div> 
  </div><!-- bg-video-->
</div><!-- videoPlayer -->
      <div id="socialFeed">

            <div class="container">
                <div class="col-md-12 col-xs-12">
                    <h3> Campus Buzz</h3>

                    <div id="newsfeed">
                      <div class="container">
                        <div class="col-md-12 col-xs-12">
                          <a class="more" href="http://loyno.edu/news">newsroom</a>
                        </div>
                      </div>
                    </div>
                </div>
            </div>

        </div>
        <!--socialFeed-->

        <div id="newOrleans" class="full-width-bg">
            <div class="container">
            <div class="col-lg-12 col-md-12">
                <h3 class="big-school-feel white">
                  <div class="headingLine">Our Campus Is</div>
                  <div class="headingLine text-style-1">New Orleans</div>
                </h3>

                <div class="main">
                    <p>Authentic, innovative, and completely one of a kind.</p>

                    <div class='action'>
                        <ul>
                            <li><a href="transportation-safety.php">Campus Map</a>
                            </li>
                            <li><a href="transportation-safety.php#safety">Safety</a>
                            </li>
                            <li><a href="transportation-safety.php#transportationPolicy">Transportation</a>
                            </li>

                        </ul>

                        <p><a class="more" href="neworleans.php">more about new orleans</a>
                        </p>

                    </div>
                    <!--endaction-->
                </div>
                <div class="campus_circle hidden">
                    <p><strong>Something cool</strong> about security goes here</p></div>

                <!--endmain-->
            </div>
            </div>
        </div>
        <!--endacademics-->


        <div id="map"></div>
        <!--endmap-->

<?php 
include( 'includes/footer.php'); 
include( 'includes/more-menu.php'); 
include( 'includes/javascript.php'); 
include( 'includes/homepage-js.php');
?>

</body>
</html>