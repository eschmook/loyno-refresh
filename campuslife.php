<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Campus Life | Loyola University New Orleans</title>
    <link type="text/css" rel="stylesheet" href="http://fast.fonts.net/cssapi/d5e07912-c037-4030-b268-eb9fd671dc66.css" />
    <!-- Bootstrap -->
    <link href="./css/bootstrap.css" rel="stylesheet">
    <!--<link rel="stylesheet" type="text/css" href="styles.css"> -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="./apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="./apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="./apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="./apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="./apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="./apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="./apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="./apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="./apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="./android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="./favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="./favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="./favicon-16x16.png">
    <link rel="manifest" href="./manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="./ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- rich preview meta -->
    <meta name="description" content="Campus Life of Loyola University New Orleans" />
    <meta property="og:title" content="Loyola University New Orleans Campus Life" />
    <meta property="og:description" content="A leading Catholic, Jesuit university, Loyola offers students from all faith traditions a campus environment rich with both spirituality and academic inquiry." />
    <meta property="og:image" content="./img/loyno_havoc.jpg" />

    <link rel="stylesheet" type="text/css" href="./fonts/faith-and-glory-one/faith-and-glory-one.css">
    <link rel="stylesheet" type="text/css" href="./fonts/faith-and-glory-two/faith-and-glory-two.css">
    <link rel="stylesheet" href="./css/flickity.css" media="screen">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <?php require('includes/header.php'); ?>

    <div id="landing-campuslife" class="landingSection hero">
    <div id="whiteOverlay"></div>
        <div class="container">
            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12">
            <h2>Campus Life </h2>
                <p>Meet your new neighbors: <strong>Artists, scientists, inventors, leaders, thinkers, dreamers – you</strong>.</p>
                <p>When you’re part of the Wolf Pack, you’re constantly in action. The Loyola campus community is welcoming and active, so it’s easy for you to connect, thrive, and transform.</p>

               <a href="#linksStart" class="slowscrolling"><img src="./img/svg/arrow-down.svg" alt="arrow to read more" class="arrow_more pulse_animation" /></a>

            </div>

            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 stats">
              <div class="stat">
                <span class="fa svg-bg experiential"></span>
                  <div class="numberFact">90%</div>
                  <div>of Loyola students participate in at least one experiential learning opportunity <em>internships, service learning</em></div>
              </div>
              <div class="stat">
                  <span class="fa svg-bg clubs"></span>
                    <div class="numberFact">#130</div>
                    <div>Student organizations, clubs and intramural sports</div>
              </div>    
            </div>            
        </div>
    </div>

    <div class="landingSection" id="linksStart">
        <div class="container items">
            <div class="landcol col-md-6 col-xs-12">
                    <div class="landbox col-md-12">
                        <h4>Student Organizations</h4>

                        <ul>
                            <li><a href="http://studentaffairs.loyno.edu/cocurricular/student-organizations-directory">Student Organization Directory</a>
                            </li>
                            <li><a href="https://orgsync.com/67501/chapter">Student Government</a>
                            </li>
                            <li><a href="http://mm.loyno.edu/volunteer-service/loyola-university-community-action-program">LUCAP</a>
                            </li>
                            <li><a href="http://studentaffairs.loyno.edu/involvement/greek-life">Fraternity + Sorority Programs</a>
                            </li>
                            <li><a href="http://www.loyno.edu/cie/isa">International Student Association</a>
                            </li>
                            <li><a href="http://www.loyolamaroon.com">Student News Media</a> </li>
                        </ul>
                    </div>

                    <div class="landbox col-md-12">
                        <h4>Services</h4>
                        <ul>
                            <li><a href="http://www.bkstr.com/loyolanostore/home">Bookstore</a>
                            </li>
                            <li><a href="http://studentaffairs.loyno.edu/residential-life">Residential Life</a>
                            </li>
                            <li><a href="http://sfs.loyno.edu">Student Financial Services</a>
                            </li>
                            <li><a href="http://studentaffairs.loyno.edu/careers">Career Development Center</a>
                            </li>
                            <li><a href="http://studentaffairs.loyno.edu/counseling">University Counseling Center</a>
                            </li>
                            <li><a href="http://studentaffairs.loyno.edu/health">Student Health Services</a>
                            </li>
                            <li><a href="http://studentaffairs.loyno.edu/residential-life/campus-dining">Dining Services</a>
                            </li>
                        </ul>
                    </div>

                </div>
                <!--endleftcol-->

            <div class="landcol col-md-6 col-xs-12">
                    <div class="landbox col-md-12">
                        <h4>Athletics</h4>

                        <ul>
                            <li><a href="http://www.loyolawolfpack.com">Athletics</a>
                            </li>
                            <li><a href="http://studentaffairs.loyno.edu/sports/membership-services">University Sports Complex</a>
                            </li>
                            <li><a href="http://studentaffairs.loyno.edu/involvement/club-sports">Club Sports</a>
                            </li>
                            <li><a href="http://studentaffairs.loyno.edu/involvement/intramural-sports">Intramurals</a>
                            </li>
                            <li><a href="http://studentaffairs.loyno.edu/cocurricular/group-fitness-classes">Fitness Programs</a></li>
                            <li style="visibility: hidden;"><a></a></li>
                        </ul>
                    </div>

                    <div class="landbox col-md-12">
                        <h4>Events</h4>
                        <ul>
                            <li><a href="http://calendar.loyno.edu/">Campus Calendar</a>
                            </li>
                            <li><a href="http://studentaffairs.loyno.edu/orientation">New Student Orientation</a>
                            </li>
                            <li><a href="http://studentaffairs.loyno.edu/residential-life/family-weekend">Family Weekend</a>
                            </li>
                            <li><a href="http://alumni.loyno.edu/lenten-series">Lenten Lecture Series</a>
                            </li>
                            <li><a href="http://alumni.loyno.edu/wolves-prowl">Wolves on the Prowl</a>
                            </li>
                            <li><a href="http://alumni.loyno.edu/spring-crawfish-boils">Senior Crawfish Boil</a>
                            </li>
                            <li><a href="http://www.loyno.edu/commencement/">Commencement</a>
                            </li>
                        </ul>
                    </div>

                </div>
                <!--endrightcol-->

        </div>
    </div>

<div id="landingbottom">
    <div class="container">

        <div class="landbox col-md-6 col-xs-12">
            <ul>
                <li><a href="http://studentaffairs.loyno.edu/ ">Student Affairs Home</a>
                </li>
                <li><a href="http://studentaffairs.loyno.edu/residential-life">Residential Life Home</a>
                </li>
            </ul>
        </div>
    </div>
</div>

<?php 
    include( 'includes/footer.php'); 
    include( 'includes/more-menu.php'); 
    include( 'includes/javascript.php'); 
?>

</body>
</html>