<!-- footer -->
<footer>
  <div class="fullwidth" id="footer">
        <div class="container-fluid topFooter">
            <div class="col-xl-3 offset-xl-3 col-lg-3 col-md-3 col-xs-12 nopadding">
                <a href="http://www.loyno.edu/"><img class="loynoLogo" src="./img/svg/Loyno_Marquette_vertical.svg" alt="Loyola University"></a>
            </div>
            <div class="col-xl-3 col-lg-6 col-md-6 col-xs-12 footer-copy">
                <p>Copyright &copy; 1996-<?php echo date( 'Y');?> Loyola University New Orleans
                    <br /> 6363 St. Charles Avenue | New Orleans, LA 70118
                    <br />
                    <a href="http://apply.loyno.edu/">Admissions</a>: 504-865-3240 or 1-800-456-9652
                    <br />
                    <br />
                    <span class="help">
                        <a href="mailto:admit@loyno.edu">Contact Us</a><br /> 
                        <a href="http://finance.loyno.edu/emergency" class="emergency_info">Emergency Information</a>
                    </span>
                </p>
            </div>
            <div class="col-xl-3 offset-xl-2 col-lg-6 col-md-6 col-xs-12">
               <a href="http://campaign.loyno.edu/?utm_source=loyola-homepage&utm_medium=website&utm_campaign=Campaign%20Link%20on%20Loyola%20homepage%20footer">
                <img class=" " src="./img/svg/FIF_LUNO_Logo_cntr.svg" alt="Faith in the Future logo">
                </a>
            </div>

        </div>

        <div class="container-fluid bottomFooter">
            <div class="col-xl-3 offset-xl-3 col-lg-3 col-md-3">
                <h5>Popular Searches</h5>
                  <ul>
                    <li><a href="http://www.loyno.edu/jump/about/visitors/map-loyola.php">Campus Maps</a></li>
                    <li><a href="http://finance.loyno.edu/student-finance/tuition-fees">Tuition</a></li>
                    <li><a href="http://studentaffairs.loyno.edu/residential-life/residence-halls">Housing</a></li>
                    <li><a href="http://academicaffairs.loyno.edu/records/transcripts">Transcripts</a></li>
                    <li><a href="http://finance.loyno.edu/human-resources/employment/">Employment at Loyola</a></li>
                    <li><a href="http://academicaffairs.loyno.edu/records/academic-calendars">Academic Calendars</a></li>
                    <li><a href="http://www.bkstr.com/Home/10001-108404-1">Bookstore</a></li>
                  </ul>
            </div>

            <div class="col-xl-3 col-lg-3 col-md-3">
                <h5 class="blank">&nbsp;</h5>
                  <ul>
                    <li><a href="http://studentaffairs.loyno.edu/careers">Employola</a></li>
                    <li><a href="http://studyabroad.loyno.edu/">Study Abroad</a></li>
                    <li><a href="http://academicaffairs.loyno.edu/records">Student Records</a></li>
                    <li><a href="http://finance.loyno.edu/police/parking-services">Parking Services</a></li>
                    <li><a href="http://finance.loyno.edu/human-resources">Human Resources</a></li>
                    <li><a href="http://advancement.loyno.edu/marketing">Marketing + Communications</a></li>
                  </ul>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3">
                <h5>Campus Community</h5>      
                <ul>
                    <li><a href="http://apply.loyno.edu">Admissions Office</a></li>
                    <li><a href="http://sfs.loyno.edu">Student Financial Services</a></li>
                    <li><a href="http://finance.loyno.edu/human-resources/federal-work-study-program">Federal Work Study Program</a></li>
                    <li><a href="https://lora.loyno.edu">LORA</a> | 
                        <a href="http://bulletin.loyno.edu">Bulletin</a> | 
                      <a href="https://loyno.blackboard.com">Blackboard</a> |
                      <a href="https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&hd=my.loyno.edu&sacu=1&flowName=GlifWebSignIn&flowEntry=AddSession">Student Email </a> |
                      <a href="https://secure.loyno.edu/wolfmail/src/login.php">Wolfmail</a> | <a href="http://zmail.loyno.edu">Zimbra</a></li>
                  </ul>
            </div>
            <div class="col-xl-3 col-lg-2 col-md-3 col-xs-12 footerLogos">
               <a href="http://www.ajcunet.edu/" target="_blank"><img class="" src="./img/svg/logo-ajcu.svg" alt="ajcu"></a>
            </div>

    </div> <!-- end 2nd row -->
  </div><!--endfooter-->

  </footer>