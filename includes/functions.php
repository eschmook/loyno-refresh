<?php 
  // check to see if we are on the production server
  // needed for functions calling from Nadal
  function checkProd() {
    if ($_SERVER['SERVER_NAME'] == "www.loyno.edu") {
        $prod = true;
      } else {      
        $prod = false;
      }
      return $prod;
  }  

  function debug_to_console( $data ) {
      $output = $data;
      if ( is_array( $output ) )
          $output = implode( ',', $output);

      echo "<script>console.log( 'Debug Objects: " . $output . "' );</script>";
  }

  /* this will render sliders from the included sliders.php in the root directory
  * makes life easier for semi-tecnical people to be able to update slides on the homepage
  * reduces the nees
  */ 
  function renderSlider() {
    // define arrays to be populated by the included file
    $sliderURL = array();
    $sliderImage = array();
    $sliderImageMobile = array();
    $sliderDesc = array();

    // pull array from included file
    @require_once("sliders.php");

    // see how many sliders we have
    $arrlength=count($sliderURL);

    // loop through the arrays and render out the HTML block for sliders
    for($x=0;$x<$arrlength;$x++)
      {
      echo '<div class="carousel-cell">'."\n";
      echo '<a href="'.$sliderURL[$x].'">'."\n";
      echo '  <picture class="img-responsive">'."\n";
      echo '                <source  srcset="./img/slides/'.$sliderImageMobile[$x].' 1x" media="(max-width: 767px)" />'."\n";
      echo '                <source  srcset="./img/slides/'.$sliderImage[$x].' 1x" media="(min-width: 768px)" />'."\n";
      echo '               <img src="./img/slides/'.$sliderImage[$x].'" srcset="./img/homepage/slides/'.$sliderImage[$x].' 1x, ./img/slides/'.$sliderImageMobile[$x].' .5x" size="(min-width: 800px) 50vw, 100vw" class="img-responsive" alt="'.$sliderDesc[$x].'" />'."\n";
      echo '      </picture>'."\n";
      echo '  </a>'."\n";
      echo '        <div class="carousel-caption col-xl-7 col-lg-7 col-md-7 col-xs-12">'."\n";
      echo $sliderDesc[$x].' <a href="'.$sliderURL[$x].'" class="coral-arrow"></a>'."\n";
      echo '        </div>'."\n";
      echo '      </div><!-- carousel-cell -->'."\n";
      }
  }
?>