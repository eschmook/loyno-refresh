<script>

    //---- HOMEPAGE BACKGROUND VIDEO STUFF --- \\
    var tag = document.createElement('script');
      tag.src = "https://www.youtube.com/iframe_api";
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
      var player;
      function onYouTubeIframeAPIReady() {
        player = new YT.Player('player', {
          videoId: '_87_2xbmnwI',//'amE-u4tcQ_w', //'q2UhXR99b0w', //
          //height: '750',
          playerVars: {
            enablejsapi: 1, // js calls enabled
            //origin: 'http://loyno.edu',
            autoplay: 1,
            controls: 0,
            disablekb: 1, //keyboard
            fs: 0, // fullscreen
            modestbranding: 1, // keep to 1
            loop: 1,
            rel: 0, // related videos
            showinfo: 0,
            playlist: '_87_2xbmnwI' // same as video ID for looping
          }
        });
      } 

    // ALL HOMEPAGE GMAP STUFF ~MJ

    var markers = [];

    function initMap() {
        var customMapType = new google.maps.StyledMapType([{
            stylers: [{
                hue: '#f8d905'
            }, {
                visibility: 'simplified'
            }, {
                gamma: 0.5
            }, {
                weight: 0.5
            }]
        }, {
            elementType: 'labels',
            stylers: [{
                visibility: 'off'
            }]
        }, {
            featureType: 'water',
            stylers: [{
                color: '#f8d905'
            }]
        }], {
            name: 'Custom Style',
            scaledSize: new google.maps.Size(25,25)
        });
        var customMapTypeId = 'custom_style';

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 13,
            scrollwheel: false,
            zoomControl: true,
            gestureHandling: 'cooperative',
            center: {
                lat: 29.93414,
                lng: -90.121721
            },
            mapTypeControlOptions: {
                mapTypeIds: [google.maps.MapTypeId.ROADMAP, customMapTypeId]
            }
        });
        setMarkers(map);
        map.mapTypes.set(customMapTypeId, customMapType);
        map.setMapTypeId(customMapTypeId);


    }
    // Data for the markers consisting of a name, a LatLng and a zIndex for the order in which these markers should display on top of each other. ~mj
    var locations = [        
        {   name: 'Loyola University New Orleans', 
            desc: 'You are here',
            lat: 29.936689, 
            long: -90.1213043,
            label: '1'
        },
        {   name: 'St. Charles Avenue Streetcar Line', 
            desc: 'Runs daily from New Orleans Carrollton neighborhood through the university district to New Orleans’ Central Business District.',
            lat: 29.932937, 
            long: -90.120773, 
            label: '4'
        },
        {   name: 'Audubon Park', 
            desc: '350-acre public park located across the street from Loyola',
            lat: 29.9256431, 
            long: -90.1279566,
            label: '2'
        },
        {   name: 'Freret St', 
            desc: 'Historic shopping and restaurant district', 
            lat: 29.9350075, 
            long: -90.1115234, 
            label: '3'
        },

        {   name: 'Smoothie King Center', 
            desc: 'Home of New Orleans’ NBA team, the Pelicans', 
            lat: 29.949035, 
            long: -90.082057, 
            label: '7'
        },
        {   name: 'Mercedes-Benz Superdome', 
            desc: 'Home of New Orleans’ NFL team, the Saints', 
            lat: 29.951061, 
            long: -90.081244, 
            label: '6'
        },
        {   name: 'Commander’s Palace', 
            desc: 'Notable Creole restaurant', 
            lat: 29.9288594, 
            long: -90.0864324, 
            label: '8'
        },
        {   name: 'New Orleans Museum of Art', 
            desc: 'Fine arts museum located inside New Orleans City Park', 
            lat: 29.986528, 
            long: -90.093532, 
            label: '10'
        },
        {   name: 'St. Louis Cathedral / Jackson Square',
            desc: 'Oldest cathedral in the United States located next to a park fronting the Mississippi River in the historic French Quarter', 
            lat: 29.957974, 
            long: -90.063728, 
            label: '5'
        },
        {   name: 'Preservation Hall', 
            desc: 'Music venue established to protect traditional New Orleans jazz', 
            lat: 29.958301, 
            long: -90.065393, 
            label: '11'
        },
        {   name: 'Magazine St', 
            desc: 'Art, shopping, and nightlife', 
            lat: 29.9209801, 
            long: -90.1013038, 
            label: '9'
        },
        {   name: 'French Quarter', 
            desc: 'A historic district and the oldest neighborhood in New Orleans', 
            lat: 29.954632, 
            long: -90.067828, 
            label: '12'
        },   
        {   name: 'Louis Armstrong New Orleans International Airport', 
            desc: 'A historic district and the oldest neighborhood in New Orleans', 
            lat: 29.992201, 
            long: -90.259011, 
            label: '13'
        }, 
        {   name: 'City Park', 
            desc: '1,300-acre public park that includes a botanical garden, amusement park, New Orleans Museum of Art, sculpture garden, train garden, golf, dog park, hiking, fishing, playgrounds, sports fields, boating, and biking, among other activities', 
            lat: 29.991343, 
            long: -90.098383, 
            label: '14'
        }, 
        {   name: 'Mardi Gras World', 
            desc: 'Behind the scenes look at New Orleans Mardi Gras traditions', 
            lat: 29.934658, 
            long: -90.061582, 
            label: '15'
        }, 
        {   name: 'Segnette Baseball Field', 
            desc: 'Loyola Baseball’s home field', 
            lat: 29.903462, 
            long: -90.164378, 
            label: '16'
        } 
];

    function setMarkers(map) {
        for (var i = 0; i < locations.length; i++) {
            //console.log("i = "+i);
            var location = locations[i];
            var marker = new google.maps.Marker({
                position: {
                    lat: location.lat,
                    lng: location.long
                },
                map: map,
                label: {
                    //text: labels[i % labels.length], //[0]
                    color: 'white',
                    text: location.label
                },
                icon: {
                    path: google.maps.SymbolPath.CIRCLE,
                    scale: 16,
                    fillColor: '#e5554f',
                    fillOpacity: 1,
                    strokeColor: '#e5554f',
                    strokeWeight: 5 // helps fix clickable area for marker
                },
                draggable: false,
                animation: google.maps.Animation.DROP,

            });
            //console.log("desc: "+location.desc);

            markers.push(marker);



            // open infoWindow onClick
            google.maps.event.addListener(marker, 'mouseover', (function(mm) {
                return function() {
                    //console.log("on mouseover i is "+i);
                    var index = markers.indexOf(this);
                    //console.log("this = "+this);
                    //console.log("location's index = "+index);
                    infowindow.setContent(
                        "<div class='infoheading'>"+locations[index].name+"</div>" + "<div class='infocontent'>" + locations[index].desc+"</div>"
                        );
                    infowindow.open(map, mm);

                    // change label, marker, and infoWindow color/bg color
                    var label = this.getLabel();
                        label.color = "#660000";
                        this.setLabel(label);

                    var icon = this.getIcon();
                        icon.fillColor = 'gold';
                        icon.strokeColor = 'gold';
                        this.setIcon(icon);

                    // remove the default triangle div entirely
                    var mainDiv = $('.gm-style-iw');
                        mainDiv.prev().remove();
                        mainDiv.next().remove(); 
                }
            })(marker, location[i]));

            // for touch events, it will trigger the mouseover ~mj
            google.maps.event.addListener(marker, 'mousedown', function(mm){
                google.maps.event.trigger(this, 'mouseover');
            }); 

            var infowindow = new google.maps.InfoWindow();

            google.maps.event.addListener(marker, "mouseout", function(mm) {
                var label = this.getLabel();
                label.color = "white";
                this.setLabel(label);

                var icon = this.getIcon();
                icon.fillColor = '#e5554f'; // reddish color
                icon.strokeColor = '#e5554f';
                this.setIcon(icon);

                // infoWindow will close but after a slight delay
                setTimeout(function() {
                    infowindow.close(map, mm);
                }, 350);

            });

            // for touch events, it will trigger the mouseout ~mj
            google.maps.event.addListener(marker, 'mouseleave', function(mm){
                google.maps.event.trigger(this, 'mouseout');
            }); 

            // center map on window resize
            google.maps.event.addDomListener(window, 'resize', function(mm) {
                var center = map.getCenter();
                google.maps.event.trigger(map, "resize");
                map.setCenter(center); 
            });



        }

    }
    //end all map stuff///////

</script>

<?php //  for the Twitter Web Intents on homepage  ?>
<script type="text/javascript"
        async src="https://platform.twitter.com/widgets.js" > 
</script>

<?php // for gMap on home page ?>
<script async defer src="//maps.googleapis.com/maps/api/js?v=3.27&key=AIzaSyAoTXBmi6Dda9jriF-mwpfjwXdvhVasgP8&callback=initMap">
</script>