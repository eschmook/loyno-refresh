<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="./js/flickity.pkgd.min.js"></script>
<script src="./js/bootstrap.min.js"></script>
<script src="./js/jquery.flip.min.js"></script>
<script>
    $(document).ready(function() {
        $("nav a").on("click", function() {
            $("nav").find(".active").removeClass("active");
            $(this).parent().addClass("active");
        });

        $('#subby').click(function() {
            $('#global-search-form').submit();
        });

        $('#menubutton').click(function(event) {
            $('#sideMenu').slideToggle();
        });

        $('#more_link_mobile').click(function() {
            $('#more-menu').css({
                display: 'block',
                position: 'absolute'
            });
        });

        var headerHeight = $('#header').height();

        //spacer height auto ~mj
        function checkWidth() {
            var headerHeight = $('#header');
            var $window = $(window);
            var windowsize = $window.width();
            if (windowsize) {
                $('.spacer').css({height: headerHeight.height()});
            } if (windowsize < 768) {
                $('#whiteOverlay').css({display: 'none'});
            }
        }
        // Execute on load
        checkWidth();
        // Bind event listener
        $(window).resize(checkWidth);

         // Add smooth scrolling to all links
        $("a.slowscrolling").on('click', function(event) {

          // Make sure this.hash has a value before overriding default behavior
          if (this.hash !== "") {
            // Prevent default anchor click behavior
            event.preventDefault();

            // Store hash
            var hash = this.hash;

            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
            // important to minus the headerHeight because of the spacer ~mj
            $('html, body').animate({
              scrollTop: $(hash).offset().top-headerHeight
            }, 800, function(){
         
              // Add hash (#) to URL when done scrolling (default click behavior)
              console.log($(hash).offset().top)
              window.location.hash = hash;
            });
        } // End if

});

    });

    // move side menu for mobile //
    $('window').resize(function(event) {
        /* Act on the event */

        //responsive stuff
        if ($("#mobileNav").css("display") == "block") {
            $('#sideMenu').appendTo('#mobileNav');
        } else {
            $('#sideMenu').appendTo('#menuHolder');
            //$('#sideMenu').css("display", "block");
        }
    });

    // to add a class to the current LP
    var currentPage = window.location.pathname.split('/');
    currentPage = currentPage.pop();
    //console.log('current page is ' + currentPage);

    switch (currentPage) {
        case 'about.php':
            $('.about').addClass('current')
            break;
        case 'admissions.php':
            $('.admissions').addClass('current')
            break;
        case 'campuslife.php':
            $('.campus-life').addClass('current')
            break;
        case 'neworleans.php':
            $('.neworleans').addClass('current')
            break;
        case 'academics.php':
            $('.academics').addClass('current')
            break;
        case 'jesuit-identity.php':
            $('.jesuit').addClass('current')
            break;
    };

    // More Menu-----------//////
    $('.more_link').click(function(e) {
        $('#more-menu').show("fast");
    });

    $('#more-menu-close').click(function(e) {
        $('#more-menu').hide("fast");
    });

    $('#more-menu').click(function(e) {
        $('#more-menu').hide("fast");
    });

    // hide default value on fous
    $("#search-input-desktop").focus(function(){ 
        $(this).val(''); 
     }); 
    $("#search-input-mobile").focus(function(){ 
        $(this).val(''); 
     }); 

    // ALL CAROUSELS FOR THE HOME PAGE \\
    $('.slider-carousel').flickity({
      // options
      cellAlign: 'left',
      groupCells: false,
      contain: true,
      pageDots: true,
      selectedAttraction: 0.01,
      friction: 0.15,
      adaptiveHeight: true,
      wrapAround: true,
      imagesLoaded: true,
      autoPlay: 3500
    });

    $('.facts-carousel').flickity({
      // options
      cellAlign: 'left',
      groupCells: true,
      contain: true,
      pageDots: true,
      selectedAttraction: 0.01,
      friction: 0.15,
      adaptiveHeight: true,
      wrapAround: true,
      autoPlay: false
    });

    $('.college-carousel').flickity({
      // options
      cellAlign: 'left',
      groupCells: false,
      contain: true,
      pageDots: true,
      selectedAttraction: 0.01,
      friction: 0.15,
      adaptiveHeight: true,
      wrapAround: true,
      autoPlay: false
    });

    $('.future-carousel').flickity({
      // options
      cellAlign: 'left',
      groupCells: true,
      contain: true,
      pageDots: true,
      selectedAttraction: 0.01,
      friction: 0.15,
      adaptiveHeight: true,
      wrapAround: true,
      autoPlay: false
    });

    $('.pack-carousel').flickity({
      // options
      cellAlign: 'left',
      groupCells: true,
      contain: true,
      pageDots: true,
      selectedAttraction: 0.01,
      friction: 0.15,
      adaptiveHeight: true,
      wrapAround: true,
      autoPlay: false
    });

    // to rotate the clicked accordion header
    $('.card-header').click(function() {
        //console.log('clicked card-header');
        $(this).toggleClass('opening');
    });

    // all for the overlay ~mj
    function checkScroll(el) {
        var topVal = el.outerHeight(true);
        //var bottom_of_object = el.offset().top + topVal;
        var top_of_window = $(window).scrollTop();
        var amountOff = 500 * (top_of_window / $(window).height());
        var opacity = amountOff / topVal;
        el.find('#whiteOverlay').css('opacity',opacity);
        // set height and position from the top based off hero and header selectors
        var heroHeight = $('.hero');
        var headerHeight = $('#header');
        var $window = $(window);
        var windowsize = $window.width();
        $('#whiteOverlay').css({height: '100vh', 'max-height': heroHeight.height() });
        //$('#whiteOverlay').css({top: headerHeight.height() });
        if (windowsize > 767) {
            if (top_of_window > heroHeight.height()+75) {
                $('#whiteOverlay').css({display: 'none', height: '50vh'});
            } else $('#whiteOverlay').css({display: 'table-row'});
        } //else $('#whiteOverlay').css({display: 'none'});
    }

</script>

<script> 
  $("#card").flip({
  axis: 'y',
  trigger: 'hover',
  reverse: true
  });
  $("#card2").flip({
  axis: 'y',
  trigger: 'hover',
  reverse: true
  });

</script>

<?php 
//echo 'basename is: ' . basename($_SERVER['SCRIPT_FILENAME']);

if (basename($_SERVER['SCRIPT_FILENAME']) != 'index.php'): ?>
  <script>
      $(window).scroll(function(){
          checkScroll($('.hero'));        
      });
  </script>

<?php endif; ?>