        <div id="more-menu" style="display: none;" class="nav-collapse collapse">
            <div id="more-menu-inner">
                <div class="region region-more-menu">
                    <div class="fullwidth" id="block-menu-menu-more-menu" class="block block-menu clearfix">

                        <div id="more-menu-content">
                            <ul><span class="more-menu-header">Students </span>
                                <li class="first"><a href="http://www.loyno.edu/search/atoz/">A-Z Index</a>
                                </li>
                                <li><a href="//loyno.blackboard.com/">Blackboard</a>
                                </li>
                                <li><a href="http://bulletin.loyno.edu ">Bulletin</a>
                                </li>
                                <li><a href="http://calendar.loyno.edu ">Calendar</a>
                                </li>
                                <li><a href="http://www.loyno.edu/search/ ">Find People</a>
                                </li>
                                <li><a href="//lora.loyno.edu/">LORA</a>
                                </li>
                                <li><a href="http://www.bkstr.com/loyolanostore/home/en">Bookstore</a>
                                </li>
                                <li><a href="http://library.loyno.edu/ ">Monroe Library</a>
                                </li>
                                <li><a href="//orgsync.com/login/loyola-university-new-orleans">OrgSync</a>
                                </li>
                                <li><a href="https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&hd=my.loyno.edu&sacu=1&flowName=GlifWebSignIn&flowEntry=AddSession">Student Email</a>
                                </li>
                                <li class="last"><a href="https://secure.loyno.edu/wolfmail/src/login.php">Wolfmail</a>
                                </li>
                            </ul>

                            <ul><span class="more-menu-header">Faculty/Staff </span>
                                <li class="first"><a href="http://www.loyno.edu/search/atoz/">A-Z Index</a>
                                </li>
                                <li><a href="http://academicaffairs.loyno.edu/records/academic-calendars ">Academic Calendar</a>
                                </li>
                                <li><a href="//loyno.blackboard.com/">Blackboard</a>
                                </li>
                                <li><a href="http://calendar.loyno.edu">Calendar</a>
                                </li>                                
                                <li><a href="http://www.loyno.edu/search/">Find People</a>
                                </li>
                                <li><a href="http://finance.loyno.edu/human-resources">Human Resources</a>
                                </li>
                                <li><a href="//lora.loyno.edu/ ">LORA</a>
                                </li>
                                <li><a href="http://advancement.loyno.edu/marketing ">Marketing + Communications</a>
                                </li>
                                <li><a href="http://www.loyno.edu/administrativesenate/">Staff Senate</a>
                                </li>
                                <li><a href="http://www.loyno.edu/universitysenate/ ">University Senate</a>
                                </li>
                                <li class="last"><a href="http://zmail.loyno.edu">Zimbra</a>
                                </li>
                            </ul>

                            <ul><span class="more-menu-header">Parents &amp; Families </span>
                                <li class="first"><a href="http://academicaffairs.loyno.edu/records/academic-calendars">Academic Calendar</a>
                                </li>
                                <li><a href="http://apply.loyno.edu">Admissions</a>
                                </li>
                                <li><a href="http://apply.loyno.edu/visit-loyola">Campus Tour</a>
                                </li>
                                <li><a href="http://studentaffairs.loyno.edu/residential-life/campus-dining">Dining</a>
                                </li>
                                <li><a href="http://studentaffairs.loyno.edu/residential-life ">Housing</a>
                                </li>
                                <li><a href="//lora.loyno.edu/">LORA</a>
                                </li>
                                <li><a href="http://www.loyno.edu/jump/about/visitors/map-loyola.php ">Maps</a>
                                </li>
                                <li><a href="http://www.loyno.edu/financialaid/">Scholarships + Financial Aid</a>
                                </li>
                                <li><a href="http://finance.loyno.edu/student-finance">Student Finance</a>
                                </li>
                                <li><a href="http://academicaffairs.loyno.edu/records">Student Records</a>
                                </li>
                                <li class="last"><a href="http://finance.loyno.edu/student-finance/tuition-fees">Tuition</a>
                                </li>
                            </ul>
                            <ul><span class="more-menu-header">Alumni </span>
                                <li class="first"><a href="http://alumni.loyno.edu">Alumni Association</a>
                                </li>
                                <li><a href="http://campaign.loyno.edu">Campaign for Loyola</a>
                                </li>
                                <li><a href="http://alumni.loyno.edu/chapters-alumni-groups ">Chapters + Groups</a>
                                </li>
                                <li><a href="http://calendar.loyno.edu/sites/alumni-relations/ ">Events</a>
                                </li>
                                <li><a href="http://campaign.loyno.edu/goals/funding-universitys-mission ">The Loyola Fund</a>
                                </li>
                                <li><a href="http://alumni.loyno.edu/loyola-alumni-directory-frequently-asked-questions">Update Your Information</a>
                                </li>
                                <li><a href="http://alumni.loyno.edu/volunteer ">Volunteer</a>
                                </li>
                                <li><a href="http://campaign.loyno.edu/ways-you-can-help ">Ways You Can Help</a>
                                </li>
                                <li class="last"><a href="http://magazine.loyno.edu/wolftracks ">Wolftracks</a>
                                </li>
                            </ul>
                            <ul><span class="more-menu-header">Media </span>
                                <li class="first"><a href="http://www.loyno.edu/news/staff.php ">Public Affairs Staff</a>
                                </li>
                                <li><a href="http://www.loyno.edu/experts/ ">Guide to the Experts</a>
                                </li>
                                <li><a href="http://magazine.loyno.edu ">Loyola Magazine</a>
                                </li>
                                <li><a href="http://www.loyno.edu/news ">News Releases</a>
                                </li>
                                <li><a href="http://www.flickr.com/photos/loyolanola ">Photo Galleries</a>
                                </li>
                                <li class="last"><a href="http://www.loyolamaroon.com/">Student News Media</a>
                                </li>
                            </ul>
                        </div>
                        <!-- more menu -->
                    </div>
                </div>
            </div>
            <div id="more-menu-close" class="icon icon-close"></div>
        </div>