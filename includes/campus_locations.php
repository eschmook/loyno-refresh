<?php
// This script is used on both Admissions' campus map and mobile map. 
// If this is changed test those two maps.

//ARRAY OF ALL CAMPUS LOCATIONS
//STRUCURE =  Location, Latitude, Longitude

$locations = array( 
	array("Main Campus", 29.934714, -90.12144),
	array("Broadway Campus", 29.936769, -90.12814),
	array("Academic Quad", 29.935 , -90.12126),
	array("Biever Hall", 29.937, -90.12064),
	array("Bobet Hall", 29.93526, -90.12116),
	array("Broadway Activities Center", 29.93629, -90.1279),
	array("Buddig Hall", 29.93705 , -90.11999),
	array("Cabra Hall", 29.93693, -90.12745),
	array("Carrollton Hall", 29.93671, -90.11987),
	array("College of Law", 29.93692, -90.12857),
	array("Communications / Music Complex", 29.93378, -90.12086),
	array("Danna Student Center", 29.936, -90.12054),
	array("Greenville Hall", 29.93674, -90.12794),
	array("Monroe Library", 29.93545, -90.12006),
	array("Marquette Hall", 29.93472, -90.12145),
	array("Mercy Hall", 29.93712, -90.11889),
	array("Miller Hall", 29.93507, -90.12008),
	array("Monroe Hall", 29.93422, -90.1204),
	array("Most Holy Name of Jesus", 29.93454, -90.12201),
	array("Palm Court", 29.93435, -90.12096),
	array("Peace Quad", 29.9357, -90.12085),
	array("Residential Quad", 29.93677, -90.1203),
	array("St. Mary's Hall/Visual Arts", 29.93577, -90.1283),
	array("Stallings Hall", 29.93515, -90.12164),
	array("Thomas Hall", 29.93404, -90.1214),
	array("University Police", 29.93681, -90.12077),
	array("University Sports Complex", 29.93754, -90.11978)				  
); 


if($_POST['building'] != "" ) {
	foreach($locations as $local) {
		if($_POST['building'] == str_replace("'", '', $local[0])) {
			$lat = $local[1];
			$lng = $local[2];
			echo $lat."|".$lng;  // value passed to js
		}
	}
} else {
	$lat = "29.934714";
	$lng = "-90.12144";
}
//echo $lat."|".$lng;
?>