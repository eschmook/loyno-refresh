<header>
        <div id="mobileNav" class="container">
            <div class="mobileRow">
                <div id="mobLogo" class="col-xs-11"><a href="/"><img src="./img/svg/logo-loyola-horizontal.svg" alt="Loyola University" /></a>
                </div>
                <div id="menubutton" class="col-xs-1"><span class="fa fa-bars"></span>
                </div>
            </div>
        </div>
        <!--endmobilelogo-->
        <div id="header" class="clearfix">

            <div id="desktopNav">

                <div class="container-fluid top_nav">
                    <div class="col-lg-5 col-md-6 col-sm-12 nopadding">
                        <a class="desktop_logo" href="/">
                            <img class="img-responsive" src="./img/svg/logo-loyola-horizontal.svg" />Loyola University New Orleans Logo
                        </a>
                    </div>

                    <div class="col-lg-7 col-md-6 col-sm-12 nopadding">
                        <nav role="navigation" class="col-md-8">
                            <ul>
                                <li><a href="http://www.loyno.edu/search/">Find People</a>
                                </li>
                                <li><a href="http://www.loyno.edu/search/atoz/">A-Z Index</a>
                                </li>
                                <li><a href="#" class="more_link" id="more_link">Quick Links</a>
                                </li>
                            </ul>
                        </nav>

                        <div id="search" class="col-md-3 nopadding">
                            <form action="http://www.loyno.edu/search/results/" id="global-search-form" class="desktop-search-form">
                                <input type="hidden" name="cx" value="001928029594867018291:t598ogzt9d8" />
                                <input type="hidden" name="cof" value="FORID:9" />
                                <input type="hidden" name="ie" value="UTF-8" />
                                <input name="q" id="search-input-desktop" type="text" value="Search"><span id="subby" class="fa fa-search"></span>
                            </form>
                        </div>
                        <!--endsearch-->
                    </div>

                </div>
                <!-- top_nav -->

                <div class="row secondary_nav">
                    <nav role="navigation">
                        <ul>
                            <li><a href="./about.php" class="about">About</a>
                            </li>
                            <li><a href="./academics.php" class="academics">Academics</a>
                            </li>
                            <li><a href="./admissions.php" class="admissions">Admissions</a>
                            </li>
                            <li><a href="./campuslife.php" class="campus-life">Campus Life</a>
                            </li>
                            <li><a href="./jesuit-identity.php" class="jesuit">Jesuit Identity</a>
                            </li>
                            <li><a href="http://alumni.loyno.edu">Alumni</a>
                            </li>

                        </ul>
                    </nav>
                </div>

            </div>
            <!--enddesktopNav-->
        </div>
        <!---endheader-->

        <div id="menuHolder">
            <div id="sideMenu" class="alert-activated">
                <!-- will add this class if alert is activated via jquery -->
                <div class="row secondary_nav_mobile">
                    <nav role="navigation">
                        <ul>
                            <li><a href="./about.php">About</a>
                            </li>
                            <li><a href="./academics.php">Academics</a>
                            </li>
                            <li><a href="./admissions.php">Admissions</a>
                            </li>
                            <li><a href="./campuslife.php">Campus Life</a>
                            </li>
                            <li><a href="./jesuit-identity.php">Jesuit Identity</a>
                            </li>
                            <li><a href="http://alumni.loyno.edu">Alumni</a>
                            </li>
                        </ul>
                    </nav>
                </div>

                <ul id="sidenav" class="arrow">
                    <li>
                        <a href="http://apply.loyno.edu/majors-degrees-programs">Majors      
                      </a>
                    </li>
                    <li>
                        <a href="http://apply.loyno.edu/visit-loyola">Visit
                      </a>
                    </li>
                    <li>
                        <a href="http://apply.loyno.edu/how-apply">Apply 
                      </a>
                    </li>
                    <li class="req-info">
                        <a href="https://admissions.loyno.edu/register/inquiryform">Request Info
                      </a>
                    </li>
                </ul>

                <div id="social-links-wrapper">
                    <div class="social-links">
                        <a class="fa fa-facebook social-icon" href="http://www.facebook.com/loyno" target="_blank"></a>
                        <a class="fa fa-youtube social-icon" href="https://www.youtube.com/user/LoyolaNOLA" target="_blank"></a>
                        <a class="fa fa-twitter social-icon" href="http://twitter.com/loyola_NOLA" target="_blank"></a>
                        <a class="fa fa-instagram social-icon" href="http://instagram.com/loyola_nola/" target="_blank"></a>
                    </div>
                    <a href="http://blogs.loyno.edu" class="social-links-text">connect</a>
                </div>
                <a href="https://spark.loyno.edu/give" class="giveNow">Give Now</a>

                <div id="findPeopleNav" class="hidden-lg-up">
                    <nav role="navigation">
                        <ul>
                            <li><a href="http://www.loyno.edu/search/">Find People</a>
                            </li>
                            <li><a href="http://www.loyno.edu/search/atoz/">A-Z Index</a>
                            </li>
                            <li><a href="#" id="more_link_mobile" class="more_link toggle-nav">Quick Links</a>
                            </li>
                        </ul>
                    </nav>
                    <div id="search" class="mobileNav">
                        <div class="mobileSearch">
                            <form action="http://www.loyno.edu/search/results/" id="global-search-form" class="mobile-search-form">
                                <input type="hidden" name="cx" value="001928029594867018291:t598ogzt9d8">
                                <input type="hidden" name="cof" value="FORID:9">
                                <input type="hidden" name="ie" value="UTF-8">
                                <input name="q" id="search-input-mobile" type="text" value="Search"><span id="subby" class="fa fa-search"></span>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
            <!--endsideMenu-->

        </div>
        <!--endmenuholder-->
    </header>
    <div class="spacer"></div>