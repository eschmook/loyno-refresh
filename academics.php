<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Academics | Loyola University New Orleans</title>
    <link type="text/css" rel="stylesheet" href="http://fast.fonts.net/cssapi/d5e07912-c037-4030-b268-eb9fd671dc66.css" />
    <!-- Bootstrap -->
    <link href="./css/bootstrap.css" rel="stylesheet">
    <!--<link rel="stylesheet" type="text/css" href="styles.css"> -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="./apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="./apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="./apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="./apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="./apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="./apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="./apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="./apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="./apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="./android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="./favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="./favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="./favicon-16x16.png">
    <link rel="manifest" href="./manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="./ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- rich preview meta -->
    <meta name="description" content="Academics of Loyola University New Orleans" />
    <meta property="og:title" content="Loyola University New Orleans Academics" />
    <meta property="og:description" content="A leading Catholic, Jesuit university, Loyola offers students from all faith traditions a campus environment rich with both spirituality and academic inquiry." />
    <meta property="og:image" content="./img/loyno_havoc.jpg" />

    <link rel="stylesheet" type="text/css" href="./fonts/faith-and-glory-one/faith-and-glory-one.css">
    <link rel="stylesheet" type="text/css" href="./fonts/faith-and-glory-two/faith-and-glory-two.css">
    <link rel="stylesheet" href="./css/flickity.css" media="screen">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <?php require('includes/header.php'); ?>

    <div id="landing-academics" class="landingSection hero">
    <div id="whiteOverlay"></div>
        <div class="container">
            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12">
            <h2>Academics </h2>
                <p>
                A degree from Loyola will give you an edge as you begin your career––<strong>and the momentum to carry you forward for years to come.</strong></p>
                <p>Our students and graduates are the best and the brightest: receiving awards such as Rhodes, Fulbright and Mitchell scholarships and fellowships. Our professors hail from all over the world and bring with them real-world experience in their fields. </p>

                <a href="#linksStart" class="slowscrolling"><img src="./img/svg/arrow-down.svg" alt="arrow to read more" class="arrow_more pulse_animation" /></a>

            </div>

            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 stats">
              <div class="stat">
                <span class="fa svg-bg users"></span>
                  <div class="numberFact">10:1</div>
                  <div>student to faculty ratio </div>
              </div>

              <div class="stat">
                  <span class="svg-bg diploma"></span>
                    <div class="numberFact">Top 20</div>
                    <div>Fulbright Producer in 2015-2016 with a 20% application acceptance rate in 2017.</div>
              </div>
            </div>            
            
        </div>
    </div>

    <div class="landingSection" id="linksStart">
        <div class="container items">
                <div class="landcol col-md-6 col-xs-12">
                    <div class="landbox col-md-12">
                        <h4>Programs</h4>

                        <ul>
                            <li><a href="http://apply.loyno.edu/majors-degrees-programs">Undergraduate Programs</a>
                            </li>
                            <li><a href="http://gps.loyno.edu/graduate-programs-overview">Graduate Programs</a>
                            </li>
                            <li><a href="http://cas.loyno.edu/part-time/programs-study">Adult Part-Time Programs</a>
                            </li>
                            <li><a href="http://www.loyno.edu/cie/">International Education</a>
                            </li>
                            <li><a href="http://www.loyno.edu/loyola-core">Loyola Core</a>
                            </li>
                            <li><a href="http://academicaffairs.loyno.edu/advising">Advising</a> </li>
                            <li><a href="http://www.loyno.edu/engage/service-learning">Service Learning</a> </li>
                            <li><a href="http://academicaffairs.loyno.edu/fye/">First Year Experience</a> </li>
                       </ul>

                    </div>

                </div>
                <!--endleftcol-->

                <div class="landcol col-md-6 col-xs-12">
                    <div class="landbox col-md-12">
                        <h4>Colleges</h4>

                        <ul>
                            <li><a href="http://cas.loyno.edu">College of Arts &amp; Sciences</a>
                            </li>
                            <li><a href="http://www.business.loyno.edu/ ">College of Business</a>
                            </li>
                            <li><a href="http://gps.loyno.edu">College of Graduate and Professional Studies: </a>
                            </li>
                            <li><a href="http://law.loyno.edu/">College of Law</a>
                            </li>
                            <li><a href="http://cmfa.loyno.edu/">College of Music + Fine Arts</a>
                            </li>

                        </ul>
                    </div>

                    <div class="landbox col-md-12">
                        <h4>Schedules + Records</h4>

                        <ul>
                            <li><a href="http://academicaffairs.loyno.edu/records/academic-calendars/">Academic Calendar</a>
                            </li>
                            <li><a href="http://bulletin.loyno.edu/">Bulletin</a>
                            </li>
                            <li><a href="http://www.loyno.edu/records/academic.calendar/">Exam Schedules</a>
                            </li>
                            <li><a href="http://www.loyno.edu/commencement/">Commencement</a>
                            </li>
                            <li><a href="http://academicaffairs.loyno.edu/records/">Student Records</a>
                            </li>
                        </ul>
                    </div>

                </div>
                <!--endrightcol-->

            </div>
        </div>
    </div>

    <div id="landingbottom">
        <div class="container">
            <div class="landbox col-md-6 col-xs-12">

                <ul>
                    <li><a href="http://academicaffairs.loyno.edu/records/">Student Records</a>
                    </li>
                    <li><a href="http://academicaffairs.loyno.edu/ ">Academic Affairs Home</a>
                    </li>
             
                </ul>
            </div>
        </div>
    </div>
    <!--end loynoFacts-->

<?php 
    include( 'includes/footer.php'); 
    include( 'includes/more-menu.php'); 
    include( 'includes/javascript.php'); 
?>

</body>
</html>